<?php

/*ini_set('session.gc_maxlifetime', ($timeout * 60)); 
ini_set('session.use_strict_mode', true); // aceitar apenas sessões criadas pelo módulo session
ini_set('session.use_cookies', true); // usar junto com use_only_cookies
ini_set('session.use_only_cookies', true); // cookies gerados apenas pelo proprio usuário
ini_set('session.cookie_httponly', true); // cookies só acessíveis por HTTP (não JS)
ini_set('session.cookie_secure', false); // cookies só acessíveis por HTTPS
ini_set('session.hash_function', 'sha512'); // criptografa session: dificulta Session Hijacking       
ini_set('session.use_trans_sid', false); // suporte a SID transparente desabilitado
ini_set('session.referer_check', 'http://www.intelblock.com.br'); // checa o referer
ini_set('session.cache_limiter', 'nocache'); // não fazer cache
session_regenerate_id(); // renova ID da seção*/
date_default_timezone_set('America/Sao_Paulo');
session_start(); // IMPORTANTE: ao final dos comandos acima

include('conn.php');

$ban = mysql_query("SELECT * FROM bans WHERE usuario = '".$_SESSION['b']."' OR ip = '".$_SESSION['i']."'");
$res = mysql_fetch_array($ban);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Logue-se</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" id="parte1">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method="post">
					<span class="login100-form-title p-b-48">
						<!--<i class="zmdi zmdi-font"></i>-->
						<img src="images/logoteste.png" style="width:90%;">
						<h3 style="font-weight:none;">Você foi banido!</h3>
						<p style="text-align:left; font-family:Calibri; font-size:17px; padding:10px; margin-bottom:-30px;">
							<span style="color: #FF0933;">Banimento:</span> <?php echo $res['banimento']; ?> <br>
							<span style="color: #FF0933;">Tempo de banimento:</span> <?php echo $res['desbanimento']; ?> <br>
							<span style="color: #FF0933;">Motivo:</span> <?php echo $res['motivo']; ?><br>
						</p>
					</span>

				</form>
			</div>
		</div>
	</div>

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

	<script>
	$('#nick').on('keyup',function(){
		var valor = $("#nick").val();
		var sbs = valor.replace("'", "");
		$("#nick").val(sbs);
		//alert($("#nick").val());
	});
	/*$(document).on('click', '#criarconta', function()
	{
		$("#parte1").hide("slow");
		$("#parte2").show("slow");
	});

	$(document).on('click', '#voltar', function()
	{
		$("#parte2").hide("slow");
		$("#parte1").show("slow");
	});
	$("#formcad").submit(function(event) 
	{
		var formDados = new FormData($(this)[0]);

		$.ajax({
		url: 'cadastrar.php',
		type: 'POST',
		data: formDados,
		cache: false,
		contentType: false,
		processData: false,
		sucess: function(data)
		{	 
		},
		dataType: 'html'
		});
		 
		
		$("#parte2").hide("slow");
		$("#parte1").show("slow");
		return false;
	});*/
	</script>

</body>
</html>