<?php

session_start();
include("conn.php");

function mensagem_selecionada($pag)
{
	if($pag == $_GET['buscar'])
	{
		$class = 'active';
	} else {
		$class = null;
	}
	return $class;
}

?>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
<style>body { background-image: url(images/chat/fundo.jpg); padding:10px;}</style>

<!---->

<div class="row no-gutters">
	<div class="col-12 col-sm-6 col-md-8">
	<?php

		if(isset($_GET['buscar'])) 
		{ 
			$cliente = $_GET['buscar']; 
			$_SESSION['buscar'] =  $_GET['buscar']; 
		} else { 
			$cliente = $_SESSION['usuario']; 
		}
		$SQL = pg_query("SELECT * FROM s_administradores.mensagens WHERE cliente = '".$cliente."' ORDER BY id ASC");
		if(pg_num_rows($SQL) == 0)
		{
			if(isset($_GET['buscar'])) 
			{ 
				echo '
				<div style="background-color:white; width:80%; float:right; margin-right:30px; margin-bottom:4px; height:auto; display:table; padding:10px; border-radius:5px;">
					<img src="images/chat/seta2.png" style="width:18px; height:18px; position:relative; float:left; left:-27px; top:-11px;">
					<div class="header"><strong class="pull-left primary-font" style="color:#21ADDB;">Bem vindo</strong></div>
					<div style="clear:both;"></div>
					<p style="position:relative; top:6px; left:10px;">Ficou com dúvidas? Converse conosco através deste chat.<br>Atendimento de Segunda a Sabádo, 07:00h até às 17:30h</p>
				</div>
				';
			} else {
				echo '
				<div style="background-color:white; width:80%; float:right; margin-right:30px; margin-bottom:4px; height:auto; display:table; padding:10px; border-radius:5px;">
					<img src="images/chat/seta2.png" style="width:18px; height:18px; position:relative; float:left; left:-27px; top:-11px;">
					<div class="header"><strong class="pull-left primary-font" style="color:#21ADDB;">Bem vindo</strong></div>
					<div style="clear:both;"></div>
					<p style="position:relative; top:6px; left:10px;">Selecione um aluno para iniciar o chat.</p>
				</div>
				';
			}
		}
		
		while($Res = pg_fetch_array($SQL))
		{
			if($Res['tipo'] == 'cliente')
			{
				echo '
				<div style="background-color:#E2FFC7; width:90%; margin:0 auto; margin-bottom:4px; height:auto; display:table; padding:10px; border-radius:5px;">
					<img src="images/chat/seta1.png" style="width:18px; height:18px; position:relative; float:right; left:27px; top:-11px;">
					<div class="header"><strong class="pull-left primary-font">'.$Res['cliente'].'</strong><small class="text-muted" style="float:right;"></small></div>
					<div style="clear:both;"></div>
					<p style="position:relative; top:6px; left:10px;">'.$Res['mensagem'].'</p>
				</div>
				';
			} 
			else 
			{
				echo '
				<div style="background-color:white; width:80%; float:right; margin-right:30px; margin-bottom:4px; height:auto; display:table; padding:10px; border-radius:5px;">
					<img src="images/chat/seta2.png" style="width:18px; height:18px; position:relative; float:left; left:-27px; top:-10px;">
					<div class="header"><strong class="pull-left primary-font" style="color:#21ADDB;">Suporte Técnico</strong><small class="text-muted" style="float:right;"></small></div>
					<div style="clear:both;"></div>
					<p style="position:relative; top:6px; left:10px;">'.$Res['mensagem'].'</p>
				</div>
				';
			}
		}
	?>
	</div>  
 
	<div class="col-6 col-md-4">
		<ul class="list-group" style="position:fixed; width:30%;">
		<?php
			$sql = pg_query("SELECT * FROM s_administradores.mensagens WHERE tipo = 'cliente' ORDER BY id DESC");
			$aux = null;

			while($res = pg_fetch_array($sql))
			{
				if($res['cliente'] != $aux)
				{
					/*$contar = pg_query("SELECT * FROM mensagens WHERE tipo = 'cliente' AND cliente = '".$res['cliente']."'");
					$ultima_msg = pg_query("SELECT * FROM mensagens WHERE tipo = 'admin' AND cliente = '".$res['cliente']."' ORDER BY id DESC");
					if(pg_num_rows($ultima_msg) != 0)
					{
						$pendentes = '<span class="badge badge-primary badge-pill">'.pg_num_rows('para que toda ').'</span>';
					} else {

					}*/
					echo '<a href="chat_mensagens.php?buscar='.$res['cliente'].'" style="text-decoration:none;"><li class="list-group-item d-flex justify-content-between align-items-center '.mensagem_selecionada($res['cliente']).'">'.$res['cliente'].'</li></a>';
				}
				$aux = $res['cliente'];
			}
		?>
		</ul>
	</div>

</div>