<?php

date_default_timezone_set('America/Sao_Paulo');
session_start(); // IMPORTANTE: ao final dos comandos acima

if(!isset($_SESSION['us']))
{
	echo '<script language="JavaScript">window.location="../index.php";</script>';
} 

include("../conn.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="stylesheet" href="css/jquery-ui.css">

    <script>
        lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
        })
    </script>
    
    <style>
    .half {
        width: 100%;
    }
    .tab {
        position: relative;
        overflow: hidden;
    }
    .tab input {
        position: absolute;
        opacity: 0;
        z-index: -1;
    }
    .tab label {
        position: relative;
        display: block;
        padding: 0 0 0 1em;
        background: #333;
        line-height: 3;
        width: 100%;
        cursor: pointer;
        color: #fff;
    }
    .blue label {
        background: #2980b9;
    }
    .tab-content {
    
        overflow: hidden;
        background: #fff;
        -webkit-transition: max-height .35s;
        -o-transition: max-height .35s;
        transition: max-height .35s;
        margin-top:-7px;
        border:1px solid #ccc;
    }
    .blue .tab-content {
        background: #333;
    }
    .tab-content p {
    }
    /* :checked */
    .tab input:checked ~ .tab-content {
        max-height: 100vh;
    }
    /* Icon */
    .tab label::after {
        position: absolute;
        right: 0;
        top: 0;
        display: block;
        width: 3em;
        height: 3em;
        line-height: 3;
        text-align: center;
        -webkit-transition: all .35s;
        -o-transition: all .35s;
        transition: all .35s;
    }
    .tab input[type=checkbox] + label::after {
        content: "+";
    }
    .tab input[type=radio] + label::after {
        content: "\25BC";
    }
    .tab input[type=checkbox]:checked + label::after {
        transform: rotate(315deg);
    }
    .tab input[type=radio]:checked + label::after {
        transform: rotateX(180deg);
    }
    #screenshots img { 
        filter: brightness(0.40);
        border-radius:10px;
    }

    #screenshots img:hover { 
        filter: brightness(1);
        transition-delay: 0.1s;
    }
    </style>
    <title>Relatório geral</title>
</head>

<body style="background-color:#333;">
    <div class="container" style="background-color:white; padding:20px; margin-top:15px; border-radius:10px; width:50%; height:auto; display:table;">
        <img class="img-fluid rounded mx-auto d-block" src="../images/logoteste.png" style="width:30%;">
        <hr>
        <?php
            echo '
            <small id="emailHelp" class="form-text text-muted">Este relatório foi gerado pelo 
                <span style="color:#FF0631; font-weight:bold;">IntelBlock</span> às '.date("d/m/Y").' - '.date("H:i:m").' <span style="float:right;">
                    ';
                    if(isset($_GET['exibir']))
                    {
                        echo '<a href="relatorio_geral.php?sl='.$_GET['sl'].'&ss='.$_GET['ss'].'" id="voltar" style="margin-right:10px; color:#FF0631;"><i class="fas fa-chevron-left"></i> Voltar</a>';
                    }
                    echo '
                    <a href="javascript:void(0)" onclick="window.print();return false;" style="margin-right:10px;"><i class="fas fa-print"></i> Imprimir</a>
                </span>
            </small>';
        ?>
        <hr>
 
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <link href="css/lightbox.css" rel="stylesheet">
    <script src="js/lightbox.js"></script>
    
    <script>
    $( function() {
        $( "#accordion" ).accordion();
    } );
    </script>

    <script>
    $('.exibir_dados').on('click',function(e)
	{
        $('#parte1').hide('fast');
        $('#parte2').show('fast');
        $('#voltar').show('fast');
	});
    $('#voltar').on('click',function(e)
	{
        $('#parte2').hide('fast');
        $('#parte1').show('fast');
        $('#voltar').hide('fast');
	});
    </script>

</body>

</html>