<?php

date_default_timezone_set('America/Sao_Paulo');
session_start(); // IMPORTANTE: ao final dos comandos acima

if(!isset($_SESSION['us']) && $_SESSION['v'] != date("d"))
{
	echo '<script language="JavaScript">window.location="../painel/index.php";</script>';
}

include("../conn.php");
include("processar.php");
 
if(isset($_GET['ativarmanutencao']))
{
	mysql_query("UPDATE gerenciamento SET status = 'false';");
	echo '<script language="JavaScript">window.location="/painel/auth_/software";</script>';
}

if(isset($_GET['desativarmanutencao']))
{
	mysql_query("UPDATE gerenciamento SET status = 'true';");
	echo '<script language="JavaScript">window.location="/painel/auth_/software";</script>';
}

if(isset($_GET['ativarlogin']))
{
	mysql_query("UPDATE gerenciamento SET login = 'true';");
	echo '<script language="JavaScript">window.location="/painel/auth_/software";</script>';
}

if(isset($_GET['desativarlogin']))
{
	mysql_query("UPDATE gerenciamento SET login = 'false';");
	echo '<script language="JavaScript">window.location="/painel/auth_/software";</script>';
}

if(isset($_GET['deslogar']))
{
	mysql_query("UPDATE usuarios SET ultimo_login = '".date("d/m/Y")." às ".date("H:i:m")."' WHERE usuario = '".base64_decode($_SESSION['us'])."'");
	
	foreach($_COOKIE as $key=>$ck){
		setcookie($key, $ck, time()-3600); 
	}
	session_destroy();
	header("Location: /painel/");
}

function identificarPag($pagina)
{
	if($_GET['p'] == $pagina)
	{
		$valor = 'class="active"';
	} else {
		$valor = '';
	}
	if($pagina == base64_encode('gu') || $pagina == base64_encode('logs') || $pagina == base64_encode('software') || $pagina == base64_encode('partidas') || $pagina == base64_encode('moderadores') || $pagina == base64_encode('moderar'))
	{
		return $valor;
	} else {
		echo $valor;
	}	
}

$sql = mysql_query("SELECT *  FROM usuarios WHERE usuario = '".base64_decode($_SESSION['us'])."'");
$res = mysql_fetch_array($sql);

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Painel do usuário</title>
	<link href="https://www.intelblock.com.br/painel/auth_/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://www.intelblock.com.br/painel/auth_/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://www.intelblock.com.br/painel/auth_/css/datepicker3.css" rel="stylesheet">
	<link rel="stylesheet" href="https://www.intelblock.com.br/painel/auth_/css/jquery-ui.css">
	<link href="https://www.intelblock.com.br/painel/auth_/css/styles.css" rel="stylesheet">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	<script>
        lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
        })
    </script>
	
	<style>
	.pricing-header {
	max-width: 700px;
	}

	.card-deck .card {
	min-width: 220px;
	}
	.onoffswitch {
    position: relative; width: 62px;
    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
	}
	.onoffswitch-checkbox {
		display: none;
	}
	.onoffswitch-label {
		display: block; overflow: hidden; cursor: pointer;
		height: 42px; padding: 0; line-height: 22px;
		border: 2px solid #E3E3E3; border-radius: 22px;
		background-color: #FFFFFF;
		transition: background-color 0.3s ease-in;
	}
	.onoffswitch-label:before {
		content: "";
		display: block; width: 22px; margin: 0px;
		background: #FFFFFF;
		position: absolute; top: 0; bottom: 0;
		right: 38px;
		border: 2px solid #E3E3E3; border-radius: 22px;
		transition: all 0.3s ease-in 0s; 
	}
	.onoffswitch-checkbox:checked + .onoffswitch-label {
		background-color: #E3545D;
	}
	.onoffswitch-checkbox:checked + .onoffswitch-label, .onoffswitch-checkbox:checked + .onoffswitch-label:before {
	border-color: #E3545D;
	}
	.onoffswitch-checkbox:checked + .onoffswitch-label:before {
		right: 0px; 
	}
	#itenspartida {
		list-style:none;
		text-align:center;
	}
	#itenspartida li {
		display:inline-table;
	}
	</style>
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation" style="z-index:99999;">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="#" style="font-size:27px; position:relative; top:4px;"><span>INTEL </span>BLOCK</a>
				<?php
				if($res['permissao'] != null)
				{
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, 'http://198.50.198.40/curl/index.php');
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					//curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
					$retorno = curl_exec ($ch);

					if($retorno == "true"){
						echo '<a class="navbar-brand" href="#" style="font-size:27px; position:relative; top:4px; float:right;">VPS está <span style="color:green;">online</span></a>';
					} else {
						echo '<a class="navbar-brand" href="#" style="font-size:27px; position:relative; top:4px; float:right;">VPS está <span style="color:red;">offline</span></a>';
					}
	
					
				}
				?>
				<!--<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
						<em class="fa fa-envelope"></em><span class="label label-danger">1</span>
					</a>
						<ul class="dropdown-menu dropdown-messages">			
							<li>
								<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
									<img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
									</a>
									<div class="message-body"><small class="pull-right">1 hora atrás</small>
										<a href="#">Nova mensagem do <strong>Suporte técnico</strong>.</a>
									<br /><small class="text-muted">12:27 - 23/08/2018</small></div>
								</div>
							</li>
							<li class="divider"></li>
							<li>
								<div class="all-button"><a href="#">
									<em class="fa fa-inbox"></em> <strong>TODAS AS MENSAGENS</strong>
								</a></div>
							</li>
						</ul>
					</li>
				</ul>-->
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<?php 
			if(isset($_GET['p']) && $_GET['p'] == base64_encode('meuperfil'))
			{
				$rand = "?".rand(0, 9999);
			}
			echo '<a href="/painel/auth_/perfil"><img src="https://intelblock.com.br/painel/auth_/og/'.$_SESSION['us'].'.jpg'.$rand.'" class="img-responsive" style="padding:5px; position:relative; top:10px; margin:0 auto; width:200px;" title="Clique na foto para alterar seus dados"></a>'; 
		?>
		<p style="text-align:center; padding-top:10px;">
		<?php
			echo "
				<span style='font-weight:bold; font-size:16px;'>".$res['nickname']."</span><br>
				<span style='font-size:14px;'>Último login: ".$res['ultimo_login']."</span>
			";
		?>
		</p>
		<div class="profile-sidebar" style='text-align:center;'>
		</div>
		<div class="divider"></div>

		<ul class="nav menu" style="font-family:Calibri; font-size:22px !important;">
			<li <?php identificarPag(base64_encode('principal'));?>><a href="/painel/auth_/principal"><i class="fas fa-home">&nbsp;</i> Principal</a></li>
			
			<?php
				$consulta_mod = mysql_query("SELECT * FROM moderadores WHERE usuario = '".base64_decode($_SESSION['us'])."'");

				if(mysql_num_rows($consulta_mod) > 0)
				{
					echo '<li '.identificarPag(base64_encode('moderar')).'><a href="/painel/auth_/moderar"><i class="fas fa-life-ring">&nbsp;</i> Moderar</a></li>';
				} 

				echo '<li '.identificarPag(base64_encode('partidas')).'><a href="/painel/auth_/partidas/usuario"><i class="fas fa-gamepad">&nbsp;</i> Partidas</a></li>';
				
				if($res['moedas'] != 0)
				{
					echo '<li '.identificarPag(base64_encode('moderadores')).'><a href="/painel/auth_/moderadores"><i class="fas fa-user-secret">&nbsp;</i> Moderadores</a></li>'; 
				}
				
			?>

			
			<li <?php identificarPag(base64_encode('historico'));?>><a href="/painel/auth_/historico" id="historico"><i class="fas fa-history">&nbsp;</i> Histórico</a></li>
			<?php
				if($res['permissao'] == 1)
				{
					echo '
						<li '.identificarPag(base64_encode('gu')).'><a href="/painel/auth_/gerenciar/1/0"><i class="fas fa-cogs">&nbsp;</i> Gerenciar usuários</a></li>
						<li '.identificarPag(base64_encode('software')).'><a href="/painel/auth_/software"><i class="fas fa-cogs">&nbsp;</i> Gerenciar software</a></li>
						<li '.identificarPag(base64_encode('logs')).'><a href="/painel/auth_/logs/0"><i class="fas fa-cogs">&nbsp;</i> Logs</a></li>
					';
				}
			?>
			<!--<li class="parent "><a data-toggle="collapse" href="#sub-item-1">
				<em class="fa fa-navicon">&nbsp;</em> Multilevel <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Sub Item 1
					</a></li>
					<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Sub Item 2
					</a></li>
					<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Sub Item 3
					</a></li>
				</ul>
			</li>-->
			<?php
				if(isset($_SESSION['a1']) && $_SESSION['a1'] != "0")
				{
					echo '<li><a href="/painel/auth_/index.php?voltar"><em class="fa fa-power-off">&nbsp;</em> Voltar para o administrador</a></li>';
				} else {
					echo '<li><a href="/painel/auth_/deslogar"><em class="fa fa-power-off">&nbsp;</em> Sair do sistema</a></li>';
				}
			?>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header"></h1>
			</div>
		</div>

		<?php
		

		if(isset($_GET['p']) && $_GET['p'] == base64_encode('criar_partida'))
		{
			$sql = mysql_query("SELECT * FROM usuarios WHERE usuario = '".base64_decode($_SESSION['us'])."'");
			$res = mysql_fetch_array($sql);

			echo '
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<span style="float:left;">Criar uma nova partida</span> 
						<a href="/painel/auth_/partidas/usuario" class="btn btn-md btn-info" style="float:right;">Voltar</a>
					</div>
					<div class="panel-body">
						<form method="post">
							<div class="form-group col-md-3">
								<label for="exampleInputEmail1">Nome da partida</label>
								<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="nome">
							</div>
							<div class="form-group col-md-4" style="position:relative; top:6px;">
								<label for="exampleInputEmail1">Jogo</label>
								<select class="form-control form-control-lg" name="jogo">
									<option selected disabled>Selecione</option>
									<option value="PointBlank">Point Blank</option>
									<option value="Zula">Zula</option>
									<option value="CrossFire">Cross Fire</option>
									<option value="BloodStrike">BloodStrike</option>
									<option value="BlackSquad">Black Squad</option>
									<option value="CombatArms">Combat Arms</option>
									<option value="LeagueOfLegends" disabled>League Of Legends (Em atualização)</option>
									<option value="PUBG">PUBG</option>
									<option value="KnivesOut">Knives Out</option>
									<option value="Warface">Warface</option>
								</select>
							</div>
							<div class="form-group col-md-1">
								<label>Salas</label>
							    <input type="number" min="0" max="100" name="qntSalas" value="0" class="form-control form-control-sm">
							</div>
							<input type="hidden" class="btn btn-primary" name="salvar" value="Criar partida">
							<input type="submit" class="btn btn-primary" name="salvar" value="Criar partida" style="position:relative; top:30px;">
						</form>
					</div>
				</div>
			</div>
			';
		}

		if(isset($_GET['p']) && $_GET['p'] == base64_encode('historico'))
		{

			echo '
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<span style="float:left;">Histórico</span>
					</div>
					<div class="panel-body">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Partidas</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Compras</a>
							</li>
						</ul>
						<div class="tab-content" id="myTabContent">
							<div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
							';
							 
							$sql = mysql_query("SELECT * FROM historico_partidas WHERE usuario = '".base64_decode($_SESSION['us'])."' ORDER BY id DESC") or die(Mysql_Error());
							if(mysql_num_rows($sql) == 0)
							{
								echo '<p><div class="alert bg-primary" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> Você ainda não participou de nenhuma partida!<a href="#" class="pull-right"><em class="fa fa-lg fa-close"></em></a></div></p>';
							} else {
								echo '
								<table class="table table-hover table-sm">
								<thead>
									<tr>
										<th scope="col">Partida</th>
										<th scope="col">Data</th>
									</tr>
								</thead>
								<tbody>
								';
								while($res = mysql_fetch_array($sql))
								{
									$separar = explode(';', $res['partida']);
									if(count($separar) == 1)
									{
										$nomePartida = $res['partida'];
									} else {
										$nomePartida = $separar[1];
									}
									echo '
									<tr>
										<td>'.$nomePartida.'</td>
										<td>'.$res['data'].'</td>
									</tr>
									';
									$id++;
								}
								echo '
								</tbody>
								</table>';
							}
							echo '
							</div>
							<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
							';
							$sql = mysql_query("SELECT * FROM historico_compras WHERE usuario = '".base64_decode($_SESSION['us'])."' ORDER BY id DESC") or die(Mysql_Error());
							if(mysql_num_rows($sql) == 0)
							{
								echo '<p><div class="alert bg-primary" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> Você ainda não realizou nenhuma compra!<a href="#" class="pull-right"><em class="fa fa-lg fa-close"></em></a></div></p>';
							} else {
								echo '
								<table class="table table-hover table-sm">
								<thead>
									<tr>
										<th scope="col">Plano</th>
										<th scope="col">Valor R$</th>
										<th scope="col">Tipo de pagamento</th>
										<th scope="col">Data de compra</th>
										<th scope="col">Vencimento</th>
									</tr>
								</thead>
								<tbody>
								';
								while($res = mysql_fetch_array($sql))
								{
									echo '
									<tr>
										<td>'.$res['plano'].'</td>
										<td>'.$res['valor'].'</td>
										<td>'.$res['tipo_pagamento'].'</td>
										<td>'.$res['data'].'</td>
										<td>'.$res['vencimento'].'</td>
									</tr>
									';
									$id++;
								}
								echo '
								</tbody>
								</table>';
							}
							echo '
							</div>
						</div>
					</div>
				</div>
			</div>
			';
		}

		if(isset($_GET['p']) && $_GET['p'] == base64_encode('partidas'))
		{
			echo '
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<span style="float:left;">Partidas</span> 
						<select class="form-control form-control-sm" style="margin-left:15px; width:20%; float:left;" onchange="window.open(this.value, \'_self\');">
						';

							if(!isset($_GET['listar']))
							{
								echo '<option selected disabled>Selecione o jogo</option>';
							} else {
								echo '<option selected disabled>'.$_GET['listar'].'</option>';
								$listar = "AND jogo = '".$_GET['listar']."'";
							}

							if(isset($_GET['tp']) && $_GET['tp'] == 'usuario')
							{
								$usuario = $_SESSION['us'];
							} else {
								$usuario = $_GET['tp'];
							}


						echo '
							<option value="/painel/auth_/listar/'.$usuario.'/PointBlank">Point Blank</option>
							<option value="/painel/auth_/listar/'.$usuario.'/Zula">Zula</option>
							<option value="/painel/auth_/listar/'.$usuario.'/CrossFire">Cross Fire</option>
							<option value="/painel/auth_/listar/'.$usuario.'/BloodStrike">BloodStrike</option>
							<option value="/painel/auth_/listar/'.$usuario.'/BlackSquad">Black Squad</option>
							<option value="/painel/auth_/listar/'.$usuario.'/CombatArms">Combat Arms</option>
							<option value="/painel/auth_/listar/'.$usuario.'/LeagueOfLegends" disabled>League Of Legends (Em atualização)</option>
							<option value="/painel/auth_/listar/'.$usuario.'/PUBG">PUBG</option>
							<option value="/painel/auth_/listar/'.$usuario.'/KnivesOut">Knives Out</option>
							<option value="/painel/auth_/listar/'.$usuario.'/Warface">Warface</option>
						</select>
						<a href="/painel/auth_/criarpartida/usuario" class="btn btn-md btn-info" style="float:right;">Criar uma nova partida</a>
					</div>
					
					<div class="panel-body">
					';
					$id = 1;

					$sql = mysql_query("SELECT * FROM partidas WHERE criador = '".base64_decode($usuario)."' ".$listar." ORDER BY id DESC") or die(Mysql_Error());
					if(mysql_num_rows($sql) == 0)
					{
						echo '<p><div class="alert bg-primary" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> Você não tem nenhuma partida!<a href="#" class="pull-right"><em class="fa fa-lg fa-close"></em></a></div></p>';
					} else {
						echo '
						<table class="table table-hover table-sm">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Partida</th>
								<th scope="col">Jogo</th>
							</tr>
						</thead>
						<tbody>
						';
						while($res = mysql_fetch_array($sql))
						{
							$separar = explode(';', $res['math']);
							if(count($separar) == 1)
							{
								$nomePartida = $res['math'];
							} else {
								$nomePartida = $separar[1];
							}
							echo '
							<tr>
								<td>'.$id.'</td>
								<td>'.$nomePartida.'</td>
								<td>'.$res['jogo'].'</td>
								<td><a href="/painel/auth_/salas/'.base64_encode($res['math']).'" class="btn btn-primary btn-sm"><i class="fas fa-search-plus"></i> Ver salas</a></td>
							</tr>
							';
							$id++;
						}
						echo '
						</tbody>
						</table>';
					}
					echo '
					</div>
				</div>
			</div>
			';
		}

		if(isset($_GET['p']) && $_GET['p'] == base64_encode('moderadores'))
		{
			echo '
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<form method="post">
							<span style="float:left; margin-right:10px;">Nome do usuário</span> 
							<input type="text" class="form-control form-control-sm" name="email" style="width:25%; float:left; margin-right:5px;">
							<div class="form-check form-check-inline" style="float:left; margin-right:10px;">
								<input class="form-check-input" type="checkbox" name="monitoramento">
								<label class="form-check-label" for="inlineCheckbox1" style="font-family:Calibri; font-size:14px;">Relatório geral</label>
							</div>
							<div class="form-check form-check-inline" style="float:left; margin-right:10px;">
								<input class="form-check-input" type="checkbox" name="criar_partida">
								<label class="form-check-label" for="inlineCheckbox1" style="font-family:Calibri; font-size:14px;">Partidas</label>
							</div>
							<input type="hidden" name="criar_moderador" value="Adicionar moderador" class="btn btn-md btn-info">
							<input type="submit" name="criar_moderador" value="Adicionar moderador" class="btn btn-md btn-info">
						</form>
					</div>
					
					<div class="panel-body">
					';
					$id = 1;
					$sql = mysql_query("SELECT * FROM moderadores WHERE organizacao = '".base64_decode($_SESSION['us'])."' ".$listar." ORDER BY id DESC");
					if(mysql_num_rows($sql) == 0)
					{
						echo '<p><div class="alert bg-primary" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> Você não tem nenhum moderador!<a href="#" class="pull-right"><em class="fa fa-lg fa-close"></em></a></div></p>';
					} else {
						echo '
						<table class="table table-hover table-sm">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Moderador</th>
								<th scope="col">Permissões</th>
							</tr>
						</thead>
						<tbody>
						';
						while($res = mysql_fetch_array($sql))
						{
							if($res['criar_partida'] == 'on') { 
								$permissao = "Partidas";
							}
							if($res['ver_relatorio'] == 'on') { 
								$permissao = "Monitoramento";
							}
							if($res['criar_partida'] == 'on' && $res['ver_relatorio'] == 'on') { 
								$permissao = "Partidas & Monitoramento";
							}

							echo '
							<tr>
								<td>'.$id.'</td>
								<td>'.$res['usuario'].'</td>
								<td>'.$permissao.'</td>
								<td><a href="apagar_moderador/'.$res['id'].'" class="btn btn-primary"><i class="fas fa-trash-alt"></i> Apagar</a></td>
							</tr>
							';
							$id++;
						}
						echo '
						</tbody>
						</table>';
					}
					echo '
					</div>
				</div>
			</div>
			';
		}

		if(isset($_GET['p']) && $_GET['p'] == base64_encode('moderar'))
		{
			echo '
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<span style="float:left;">Área do moderador</span> 
					</div>
					
					<div class="panel-body">
					';
					$id = 1;

					
					$sql = mysql_query("SELECT * FROM moderadores WHERE usuario = '".base64_decode($_SESSION['us'])."' ".$listar." ORDER BY id DESC");

					if(mysql_num_rows($sql) == 0)
					{
						echo '<p><div class="alert bg-primary" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> Você não tem nenhuma partida!<a href="#" class="pull-right"><em class="fa fa-lg fa-close"></em></a></div></p>';
					} else {
						echo '
						<table class="table table-hover table-sm">
						<thead>
							<tr>
								<th scope="col">Organização</th>
								<th scope="col">Opções</th>
							</tr>
						</thead>
						<tbody>
						';
						while($res = mysql_fetch_array($sql))
						{
							if($res['criar_partida'] == 'on') { 
								$permissao = "Partidas";
							}
							if($res['ver_relatorio'] == 'on') { 
								$permissao = "Monitoramento";
							}
							if($res['criar_partida'] == 'on' && $res['ver_relatorio'] == 'on') { 
								$permissao = "Partidas & Monitoramento";
							}
							
							$consulta_org = mysql_query("SELECT * FROM usuarios WHERE usuario = '".$res['organizacao']."' ".$listar." ORDER BY id DESC");
							$resCO = mysql_fetch_array($consulta_org);

							echo '
							<tr>
								<td>'.$resCO['nome'].'</td>
								<td>';
								if($res['criar_partida'] == 'on' && $res['ver_relatorio'] != 'on') { 
									echo '<a href="/painel/auth_/criarpartida/'.base64_encode($res['organizacao']).'" class="btn btn-primary"><i class="fas fa-cubes"></i> Criar partida</a>';
								}
								if($res['criar_partida'] != 'on' && $res['ver_relatorio'] == 'on') { 
									echo '<a href="/painel/auth_/partidas/'.base64_encode($res['organizacao']).'" class="btn btn-primary"><i class="fas fa-tv"></i> Monitorar</a>';
								}
								if($res['criar_partida'] == 'on' && $res['ver_relatorio'] == 'on') { 
									echo '
										<a href="/painel/auth_/criarpartida/'.base64_encode($res['organizacao']).'" class="btn btn-primary" style="background-color:#5C26FF; border:1px solid #3300CC;"><i class="fas fa-cubes"></i> Criar partida</a>
										<a href="/painel/auth_/partidas/'.base64_encode($res['organizacao']).'" class="btn btn-primary" style="background:#FFFF00; color:#333; border:1px solid #BBBB00;"><i class="fas fa-tv"></i> Monitorar</a>
									';
								}
								echo '
								</td>
							</tr>
							';
							$id++;
						}
						echo '
						</tbody>
						</table>';
					}
					echo '
					</div>
				</div>
			</div>
			';
		}

		if(isset($_GET['relatorio']))
		{
			$separar = explode(';', base64_decode($_GET['pa']));
			if(count($separar) == 1)
			{
				$nomePartida = base64_decode($_GET['pa']);
			} else {
				$nomePartida = $separar[1];
			}
			
			echo '
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<span style="float:left;">Salas da partida '.$nomePartida.'</span> 
						<a href="javascript:void(0)" onclick="window.history.go(-1)" class="btn btn-md btn-info" style="float:right;">Voltar</a>
					</div>

					<div class="panel-body">
						<table class="table table-hover table-sm" style="font-family:Calibri; font-size:16px;">
						';
							$exibiu = 0;
							$sql = mysql_query("SELECT * FROM logs_anticheater WHERE sala = '".base64_decode($_GET['sl'])."' ORDER BY id DESC");
							while($res = mysql_fetch_array($sql))
							{
								if(!in_array($res['jogador'], $array))
								{
									$outraConsulta = mysql_query("SELECT * FROM logs_anticheater WHERE sala = '".base64_decode($_GET['sl'])."' AND jogador = '".$res['jogador']."' ORDER BY id DESC"); 
									$resOutra = mysql_fetch_array($outraConsulta);
									
									if(mysql_num_rows($outraConsulta) != 0)
									{
										if($exibiu == 0)
										{
											echo '
											<thead>
												<tr>
													<th scope="col">Jogador(a)</th>
													<th scope="col">Entrou em</th>
													<th scope="col">Saiu</th>
													<th scope="col">Permaneceu por</th>
													<th scope="col">Opções</th>
												</tr>
											</thead>
											<tbody>
											';
											$exibiu = 1;
										}
										if($resOutra['saiu'] == 0)
										{
											$saiu = '<img src="images/online.gif">';
										} else {
											$saiu = $resOutra['saiu'];
										}
										
										$sbsData1 = str_replace("/", "-", $resOutra['entrou']);
										$sepData1 = explode("-", $sbsData1);
										$gambiarra1 = str_replace("2018", "", $sepData1[2]);
										$timeNovo1 = "2018"."-".$sepData1[1]."-".$sepData1[0].$gambiarra1;

										$sbsData2 = str_replace("/", "-", $resOutra['saiu']);
										$sepData2 = explode("-", $sbsData2);
										$gambiarra2 = str_replace("2018", "", $sepData2[2]);
										$timeNovo2 = "2018"."-".$sepData2[1]."-".$sepData2[0].$gambiarra2;

										echo '
										<tr>
											<td>'.$resOutra['jogador'].'</td>
											<td>'.$resOutra['entrou'].'</td>
											<td>'.$saiu.'</td>
											<td>';
												if($saiu != 0)
												{
													$date1 = $timeNovo1;
													$date2 = $timeNovo2;

													$dateS1 = new \DateTime($date1);
													$dateS2 = new \DateTime($date2);

													$dateDiff = $dateS1->diff($dateS2);
													$result = $dateDiff->h . ' horas e ' . $dateDiff->i . ' minutos';
													echo $result;
												}
												echo '
											</td>
											<td><a href="javascript:void(0);" style="color:#FF0631;" class="exibir_dados" id="ed" name="'.$_GET['sl'].';'.$_GET['ss'].';'.$resOutra['usuario'].';'.$resOutra['alocamento'].'"><i class="fas fa-upload"></i> Carregar dados</a></td>
										</tr>
										';  
									} else {
										echo '<div class="alert alert-primary" role="alert" style="margin:15px;"> Não há histórico de jogadores nessa partida!</div>';
									}
									$i++;
								}
								$array[$i] = $resOutra['jogador'];
							}
							
						echo '
						</tbody>
						</table>

						<main id="info_col" style="display:none;">

							<input id="tab1" type="radio" name="tabs" checked>
							<label for="tab1">Configurações</label>
							
							<input id="tab2" type="radio" name="tabs">
							<label for="tab2">Alertas <span id="cs2" style="background-color:red; font-family:Calibri; font-weight:normal; font-size:16px; padding:2px; border-radius:5px; color:white; text-align:center; ">0</span></label></label>

							<input id="tab3" type="radio" name="tabs">
							<label for="tab3">Capturas <span id="cs1" style="background-color:red; font-family:Calibri; font-weight:normal; font-size:16px; padding:2px; border-radius:5px; color:white; text-align:center; ">0</span></label>

							<input id="tab4" type="radio" name="tabs">
							<label for="tab4">Processos <span id="cs3" style="background-color:red; font-family:Calibri; font-weight:normal; font-size:16px; padding:2px; border-radius:5px; color:white; text-align:center; ">0</span></label></label>

							<input id="tab5" type="radio" name="tabs">
							<label for="tab5">Janelas <span id="cs4" style="background-color:red; font-family:Calibri; font-weight:normal; font-size:16px; padding:2px; border-radius:5px; color:white; text-align:center; ">0</span></label></label>

							<input id="tab6" type="radio" name="tabs">
							<label for="tab6">Hist. Navegação <span id="cs5" style="background-color:red; font-family:Calibri; font-weight:normal; font-size:16px; padding:2px; border-radius:5px; color:white; text-align:center; ">0</span></label></label>

							<input id="tab7" type="radio" name="tabs">
							<label for="tab7">Hist. Downloads <span id="cs6" style="background-color:red; font-family:Calibri; font-weight:normal; font-size:16px; padding:2px; border-radius:5px; color:white; text-align:center; ">0</span></label></label>

							<section id="content1">
								<p id="ld1">
								</p>
							</section>
						
							<section id="content3">
								<div style="margin: 0 auto; margin-left:12px;" id="screenshots">
									<p id="ld2">
									</p>
								</div>
							</section>
							
							<section id="content2">
								<p id="ld3">
								</p>
							</section>

							<section id="content4">
								<p id="ld4">
								</p>
							</section>

							<section id="content5">
								<p id="ld5">
								</p>
							</section>

							<section id="content6">
								<p id="ld6">
								</p>
							</section>
						</main>
					</div>
				</div>
			</div>
			';
		}
		 
		if(isset($_GET['p']) && $_GET['p'] == base64_encode('versalas'))
		{
			$separar = explode(';', base64_decode($_GET['pa']));
			if(count($separar) == 1)
			{
				$nomePartida = base64_decode($_GET['pa']);
			} else {
				$nomePartida = $separar[1];
			}

			echo '
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<span style="float:left;">Salas da partida '.$nomePartida.'</span> 
						<a href="javascript:void(0)" onclick="window.history.go(-1)" class="btn btn-md btn-info" style="float:right;">Voltar</a>
						<a href="javascript:void(0)" id="copiarsalas" class="btn btn-md btn-danger" style="float:right; margin-right:4px;">Copiar salas</a>
					</div>

					<form method="post">
						<div class="form-group">
							<input type="text" class="form-control" name="busca" placeholder="Precisa encontrar um jogador? Digite o nickname / email completo ou parte dele" style="width:95%; margin:0 auto; position:relative; top:15px; font-family:Calibri;">
						</div>
					</form>
					
					<div class="panel-body">
						';
						
						if(isset($_POST['busca']))
						{
							$pesquisar_sala = mysql_query("SELECT * FROM presencas WHERE usuario LIKE '%".$_POST['busca']."%' OR nickname LIKE '%".$_POST['busca']."%' ORDER BY id DESC");
							if(mysql_num_rows($pesquisar_sala))
							{
								echo '
								<table class="table table-hover table-sm">
									<thead>
										<tr>
											<th scope="col">Sala</th>
											<th scope="col">Senha</th>
											<th>Opção</th>
										</tr>
									</thead>
									<tbody>
									';
									$id = 1;
								
									while($res = mysql_fetch_array($pesquisar_sala))
									{
										$online = 0;
										$offline = 0;
										$verificar_sala = mysql_query("SELECT * FROM salas WHERE nome = '".$res['sala']."' AND senha = '".$res['senha']."' AND partida = '".base64_decode($_GET['pa'])."'");
										$resVS = mysql_fetch_array($verificar_sala);

										if(mysql_num_rows($verificar_sala) > 0)
										{
											$status1 = mysql_query("SELECT * FROM logs_anticheater WHERE sala = '".$res['sala']."' AND senha = '".$res['senha']."';");
											$aux = null;

											while($rs1 = mysql_fetch_array($status1))
											{
												if($rs1['jogador'] != $aux)
												{
													if($rs1['saiu'] == 0)
													{
														$online++;
													} else {
														$offline++;
													}
												}
												$aux = $rs1['jogador'];
											}

											echo '
											<tr>
												<td>'.$res['sala'].'</td>
												<td>'.$res['senha'].'</td>
												<td><a href="relatorio_geral.php?sl='.$_GET['sl'].'&ss='.$_GET['ss'].'&u='.base64_encode($resOutra['usuario']).'&aloc='.base64_encode($resOutra['alocamento']).'&exibir" target="_blank" style="color:#FF0631;"><i class="fas fa-tv"></i> Averiguar</a></td>
											</tr>';
											$id++;
										}
									}
									echo '<textarea type="text" id="salaray" style="color:white; background-color:white; border:none; position:absolute; z-index:-1;">'.$totalSalas.'</textarea>';
									echo '
									</tbody>
								</table>
								';
							} else {
								echo '<p><div class="alert bg-primary" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> Não encontramos o jogador nesta partida!</div></p>';
							}
						} else {
							$sql = mysql_query("SELECT * FROM salas WHERE partida = '".base64_decode($_GET['pa'])."' ORDER BY id DESC");
						}
						
						if(mysql_num_rows($sql) == 0)
						{
							echo '<p><div class="alert bg-primary" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> Não há salas!<a href="#" class="pull-right"><em class="fa fa-lg fa-close"></em></a></div></p>';
						} else {
							if(!isset($_POST['busca']))
							{
								echo '
								<table class="table table-hover table-sm">
									<thead>
										<tr>
											<th scope="col">#</th>
											<th scope="col">Sala</th>
											<th scope="col">Senha</th>
											<th scope="col">Jogadores online</th>
											<th scope="col">Jogadores offline</th>
											<th scope="col">Total de players</th>
											<th>Opção</th>
										</tr>
									</thead>
									<tbody>
									';
									$id = 1;
									$i++;
								
									while($res = mysql_fetch_array($sql))
									{
										$online = 0;
										$offline = 0;

										$status1 = mysql_query("SELECT * FROM logs_anticheater WHERE sala = '".$res['nome']."' AND senha = '".$res['senha']."';");
										$aux = null;

										while($rs1 = mysql_fetch_array($status1))
										{
											if(!in_array($rs1['jogador'].";".$res['nome'], $array))
											{
												if($rs1['saiu'] == 0)
												{
													$online++;
												} else {
													$offline++;
												}
												$i++;
											}
											$array[$i] = $rs1['jogador'].";".$res['nome'];
										}

										$resVS = mysql_fetch_array($status1);

										echo '
										<tr>
											<td>'.$id.'</td>
											<td>'.$res['nome'].'</td>
											<td>'.$res['senha'].'</td>
											<td><span style="color:green; font-weight:bold;">'.$online.' online(s)</span></td>
											<td><span style="color:red; font-weight:bold;">'.$offline.' offline(s)</span></td>
											<td>'.($online+$offline).'</td>
											<td><a href="/painel/auth_/relatorio_geral.php?sl='.base64_encode($res['nome']).'&ss='.base64_encode($res['senha']).'&c='.base64_encode($res['criador']).'" target="_blank" style="color:#FF0631;"><i class="fas fa-tv"></i> Averiguar</a></td>
										</tr>';
										$totalSalas .= "Sala: ".$res['nome']." - Senha: ".$res['senha']."\n";
										$id++;
									}
									
									echo '<textarea type="text" id="salaray" style="color:white; background-color:white; border:none; position:absolute; z-index:-1;">'.$totalSalas.'</textarea>';
									echo '
									</tbody>
								</table>
								';
							}
						}
						echo '
					</div>
				</div>
			</div>
			';
		}

		if(isset($_GET['p']) && $_GET['p'] == base64_encode('gu'))
		{
			if($res['permissao'] == 1)
			{
				$quantidade = 80;
				$pagina     = (isset($_GET['pagina'])) ? (int)$_GET['pagina'] : 1;
				$inicio     = ($quantidade * $pagina) - $quantidade;


				$qrTotal    = mysql_query("SELECT id FROM usuarios") or die(mysql_error());
				$numTotal   = mysql_num_rows($qrTotal);
				$totalPagina= ceil($numTotal/$quantidade);
				
				echo '
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<span style="float:left; font-family:Calibri !important; font-size:17px; padding-right:5px;">'.$numTotal.' usuários cadastrados</span>
							<a href="/painel/auth_/gerenciar/1/0" class="btn btn-default" style="margin-right:3px;">Confirmados</a>
							<a href="/painel/auth_/gerenciar/1/1" class="btn btn-default" style="margin-right:3px;">Não confirmados</a>
							
							';
							if(isset($_POST['busca']))
							{
								$display = "visibility:hidden;";
							} 
							echo '<nav style="margin-top:-40px;"><ul class="pagination" style="float:right; position:relative; top:-18px; '.$display.'">';

							for ($i = 1; $i <= $totalPagina; $i++)
							{
								if($i == $_GET['pagina'])
								{
									echo '<li class="page-item" style="font-size:17px;"><a class="page-link" style="color:#ccc !important; font-family:Calibri;" href="">'.$i.'</a></li>';
								} else {
									echo '<li class="page-item" style="font-size:17px;"><a class="page-link" style="color:#333 !important; font-family:Calibri;" href="/painel/auth_/gerenciar/'.$i.'">'.$i.'</a></li>';
								}
							}

							echo '</ul></nav>';
							echo '
						</div>

						<form method="post">
							<div class="form-group">
								<input type="text" class="form-control" name="busca" placeholder="Digite o nickname / email completo ou parte dele" style="width:95%; margin:0 auto; font-family:Calibri;">
							</div>
						</form>

						<div class="panel-body">
							';

							if($_GET['param'] == 0) {
								$param = "WHERE status = '1'";
							} else {
								$param = "WHERE status = '0'";
							}
							if(isset($_POST['busca']))
							{
								$sql = mysql_query("SELECT * FROM usuarios WHERE usuario LIKE '%".$_POST['busca']."%' OR nickname LIKE '%".$_POST['busca']."%' ORDER BY id DESC LIMIT $inicio, $quantidade");
							} else {
								$sql = mysql_query("SELECT * FROM usuarios ".$param." ORDER BY id DESC LIMIT $inicio, $quantidade");
							}	
							
							if(mysql_num_rows($sql) == 0)
							{
								if(isset($_POST['busca']))
								{
									echo '<p><div class="alert bg-primary" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> Não foi possível encontrar esse usuário!</div></p>';
								} else {
									echo '<p><div class="alert bg-primary" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> Não há usuários cadastrados!<a href="#" class="pull-right"><em class="fa fa-lg fa-close"></em></a></div></p>';
								}
							} else {
								echo '
								<table class="table table-hover table-sm">
									<thead>
										<tr>
											<th scope="col">Usuário</th>
											<th scope="col">Nickname</th>
											<th scope="col">Opções</th>
										</tr>
									</thead>
									<tbody>
									';
									$id = mysql_num_rows($sql);
								
									while($res = mysql_fetch_array($sql))
									{
										echo '
										<tr>
											<td>'.$res['usuario'].'</td>
											<td>'.$res['nickname'].'</td>
											<td>
												<a href="/painel/auth_/index.php?p='.base64_encode('setar_compra').'&u='.base64_encode($res['usuario']).'" class="btn btn-default btn-sm">Setar plano</a>
												<a href="/painel/auth_/index.php?p='.base64_encode('entrar_c').'&u='.base64_encode($res['usuario']).'&n='.base64_encode($res['nickname']).'" class="btn btn-warning btn-sm">Entrar</a>
												<a href="/painel/auth_/index.php?p='.base64_encode('editar_usuario').'&id='.$res['id'].'" class="btn btn-primary btn-sm">Editar</a>
												';
												$consultar_ban = mysql_query("SELECT * FROM bans WHERE usuario = '".$res['usuario']."' AND nickname = '".$res['nickname']."';");
												if(mysql_num_rows($consultar_ban) == 0)
												{
													echo '<a href="/painel/auth_/index.php?p='.base64_encode('ban').'&nickname='.$res['nickname'].'&usuario='.$res['usuario'].'" class="btn btn-info btn-sm">Banir</a>';
												} else {
													echo '<a href="/painel/auth_/index.php?p='.base64_encode('desban').'&nickname='.$res['nickname'].'&usuario='.$res['usuario'].'"  class="btn btn-success btn-sm">Desbanir</a>';
												}

												if($res['status'] == 0)
												{
													echo '<a href="/painel/auth_/index.php?p='.base64_encode('confirmarconta').'&id='.$res['id'].'" class="btn btn-success btn-sm" style="margin-left:2px;">Confirmar conta</a>';
												}

												$consultar_comprovante = mysql_query("SELECT * FROM comprovantes WHERE usuario = '".base64_encode($res['usuario'])."';");
												if(mysql_num_rows($consultar_comprovante))
												{
													echo '<a href="/painel/auth_/index.php?p='.base64_encode('vercomprovantes').'&id='.$res['id'].'" class="btn btn-warning btn-sm" style="margin-left:2px;">Comprovantes</a>';
												}
												echo '
											</td>
										</tr>';
										$id--;
									}
									echo '
									</tbody>
								</table>
								';
							}

							echo '
						</div>
					</div>
				</div>
				';
			}
		}

		if(isset($_GET['p']) && $_GET['p'] == base64_encode('entrar_c'))
		{
			if($res['permissao'] == 1)
			{
				$_SESSION['a1'] = $_SESSION['us'];
				$_SESSION['a2'] = $_SESSION['nck'];
				$_SESSION['us'] = $_GET['u'];
				$_SESSION['nck'] = $_GET['n'];
				echo '<script>window.location.href="principal";</script>';
			}
		}	

		if(isset($_GET['voltar']))
		{
			if(isset($_SESSION['a1']) && $_SESSION['a1'] != "0")
			{
				$_SESSION['us'] = $_SESSION['a1'];
				$_SESSION['nck'] = $_SESSION['a2'];
				$_SESSION['a1'] = 0;
				$_SESSION['a2'] = 0;
				echo '<script>window.location.href="principal";</script>';
			}
		}	
		
		if(isset($_GET['p']) && $_GET['p'] == base64_encode('principal'))
		{
			echo '
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<span style="float:left;">Bem vindo, '.$res['nickname'].'
					</div>
					<div class="panel-body">
						';

						// Jogadores monitorados
						$screenshots = 0;
						$qntJogadoresMonitorados = 0;
						$jogadores = mysql_query("SELECT * FROM partidas WHERE criador = '".base64_decode($_SESSION['us'])."'");
						while($res_jog = mysql_fetch_array($jogadores))
						{
							$vf_jog = mysql_query("SELECT * FROM logs_anticheater WHERE partida = '".$res_jog['math']."'");
							$qntJogadoresMonitorados = ($qntJogadoresMonitorados + mysql_num_rows($vf_jog));

							while($res_logs = mysql_fetch_array($vf_jog))
							{
								$vf_contar_ss = mysql_query("SELECT * FROM usuarios WHERE usuario = '".$res_logs['usuario']."'");
								$res_contar_ss = mysql_fetch_array($vf_contar_ss);

								$screenshots = ($screenshots + $res_contar_ss['screenshots']);
							}
							break;
						}
						// Número de partidas
						$partidas = mysql_query("SELECT * FROM partidas WHERE criador = '".base64_decode($_SESSION['us'])."'");

						if($res['moedas'] > 0)
						{
							echo '
							<div class="panel panel-container">
								<div class="row">
									<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-teal panel-widget border-right">
											<div class="row no-padding"><em class="fas fa-camera-retro fa-3x" style="color:#FFD21F"></em>
												<div class="large">'.number_format($screenshots, 0, ',', '.').'</div>
												<div class="text-muted" style="font-size:17px; font-weight:normal;">screenshots</div>
											</div>
										</div>
									</div>
									<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-blue panel-widget border-right">
										<div class="row no-padding"><em class="fas fa-user-shield fa-3x" style="color:#FF0631;"></em>
												<div class="large">'.$qntJogadoresMonitorados.'</div>
												<div class="text-muted" style="font-size:17px; font-weight:normal;">jogadores monitorados</div>
											</div>
										</div>
									</div>
									<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-orange panel-widget border-right">
										<div class="row no-padding"><em class="fas fa-cubes fa-3x" style="color:#4DEBBE;"></em>
												<div class="large">'.mysql_num_rows($partidas).'</div>
												<div class="text-muted" style="font-size:17px; font-weight:normal;">eventos realizados</div>
											</div>
										</div>
									</div>
									<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
										<div class="panel panel-red panel-widget ">
										<div class="row no-padding">
												';

												if($res['moedas'] == '9999')
												{
													$qntDias = 30;

													$consultar_data = mysql_query("SELECT * FROM historico_compras WHERE usuario = '".base64_decode($_SESSION['us'])."'");
													$cd = mysql_fetch_array($consultar_data);

													$separar1 = explode("/", $cd['vencimento']);
													$separar2 = explode("/", date("d"));
													
													if($separar1[0] == $separar2[0])
													{
														$dias = $qntDias;
													} 
													else 
													{
														if($separar1[1] != $separar2[1])
														{
															$dias = abs(($separar2[0] - $separar1[0]) - 30);
														} else {
															$dias = ($separar2[0] - $separar1[0]);
														}
													}
													echo '
														<em class="fas fa-clock fa-3x" style="color:#4DD2FF;"></em>
														<div class="large">'.$dias.'</div>
														<div class="text-muted" style="font-size:17px; font-weight:normal;">dias restantes</div>
													';
												} else {
													echo '
													<em class="fas fa-donate fa-3x" style="color:orange;"></em>
														<div class="large">'.$res['moedas'].'</div>
														<div class="text-muted" style="font-size:17px; font-weight:normal;">moedas restantes</div>
													';
												}
												echo '
											</div>
										</div>
									</div>
								</div><!--/.row-->
							</div>
							';
						} else {
							echo '<div style="width:80%; height:auto; margin:0 auto; text-align:center;"><img src="images/principal.png" style="width:100%;"></div>';
						}
						
						$consulta_mod = mysql_query("SELECT * FROM moderadores WHERE organizacao = '".base64_decode($_SESSION['us'])."'");

						/*if(mysql_num_rows($consulta_mod) > 0)
						{
							echo '
							<div class="col-md-12">
								<div class="panel panel-default ">
									<div class="panel-heading">
											Histórico de moderação
										</div>
										<div class="panel-body timeline-container">
										';
											$listar = mysql_query("SELECT * FROM moderadores WHERE organizacao = '".base64_decode($_SESSION['us'])."'");
											
											echo '<ul class="timeline">';
											$i = 0;
											while($resmod = mysql_fetch_array($listar))
											{
												$info_mod = mysql_query("SELECT * FROM historico_moderacao WHERE moderador = '".$resmod['usuario']."'");
												
												if(mysql_num_rows($infomod) != 0)
												{
													while($resim = mysql_fetch_array($infomod))
													{
														//$resim = mysql_fetch_array($info_mod);

														if($resim['processo'] == 'crossfire') { $jogo = 'CrossFire'; }
														else if($resim['processo'] == 'PointBlank') { $jogo = 'PointBlank'; }
														else if($resim['processo'] == 'zula') { $jogo = 'CrossFire'; }
														else if($resim['processo'] == 'BlackSquadGame') { $jogo = 'CrossFire'; }
														else if($resim['processo'] == 'TslGame') { $jogo = 'PUBG'; }
														else if($resim['processo'] == 'hyxd') { $jogo = 'Knives Out'; }
														else if($resim['processo'] == 'Engine') { $jogo = 'Combat Arms'; }

														echo '
														<li style="font-family:Calibri;">
															<div class="timeline-badge"><i class="fas fa-cubes"></i></div>
															<div class="timeline-panel">
																<div class="timeline-heading">
																	<h4 class="timeline-title" style="font-size:25px;">Moderador '.$resim['moderador'].'</h4>
																</div>
																<div class="timeline-body">
																	<p style="font-size:19px;">'.$resim['texto'].'</p>
																</div>
															</div>
														</li>
														';

														$i++;
													}
												}
											} 
										echo '
										</ul>	
									</div>
								</div>
							</div><!--/.col-->
							';
						}*/

						if($res['permissao'] == 1)
						{
							echo '
							<div class="col-md-12">
								<div class="panel panel-default ">
									<div class="panel-heading">
											Partidas acontecendo agora
										</div>
										<div class="panel-body timeline-container">
										';
											$listar_partidas = mysql_query("SELECT * FROM membros_partida");
											if(mysql_num_rows($listar_partidas) == 0)
											{
												echo '<p><div class="alert bg-primary" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> Não há partidas!</div></p>';
											} else {
												echo '<ul class="timeline">';
											}
											$i = 0;
											while($reslp = mysql_fetch_array($listar_partidas))
											{
												if(!in_array($reslp['partida'], $list_partidas))
												{
													$info_partida = mysql_query("SELECT * FROM partidas WHERE math = '".$reslp['partida']."'");
													$resip = mysql_fetch_array($info_partida);

													$contar_membros = mysql_query("SELECT * FROM membros_partida WHERE partida = '".$reslp['partida']."'");

													$_SESSION[$reslp['partida']] = mysql_num_rows($contar_membros);

													if($resip['processo'] == 'crossfire') { $jogo = 'CrossFire'; }
													else if($resip['processo'] == 'PointBlank') { $jogo = 'PointBlank'; }
													else if($resip['processo'] == 'zula') { $jogo = 'CrossFire'; }
													else if($resip['processo'] == 'BlackSquadGame') { $jogo = 'CrossFire'; }
													else if($resip['processo'] == 'TslGame') { $jogo = 'PUBG'; }
													else if($resip['processo'] == 'hyxd') { $jogo = 'Knives Out'; }
													else if($resip['processo'] == 'Engine') { $jogo = 'Combat Arms'; }
													else if($resip['processo'] == 'ChromiumCEF') { $jogo = 'BloodStrike'; }
													else if($resip['processo'] == 'Game') { $jogo = 'Warface'; }

													echo '
													<li style="font-family:Calibri;">
														<div class="timeline-badge"><i class="fas fa-cubes"></i></div>
														<div class="timeline-panel">
															<div class="timeline-heading">
																<h4 class="timeline-title" style="font-size:25px;">'.$reslp['partida'].'</h4>
															</div>
															<div class="timeline-body">
																<p style="font-size:19px;">Partida de <b style="color:#FF0631;">'.$jogo.'</b> com <b style="color:#FF0631;">'.$_SESSION[$reslp['partida']].'</b> jogadores online.</p>
															</div>
														</div>
													</li>
													';
												}
												$list_partidas[$i] = $reslp['partida'];
												$i++;
											}
										echo '
										</ul>	
									</div>
								</div>
							</div><!--/.col-->
							';
						} else {
							echo '<h3>Partidas recentes que você participou</h3>';
							$query = mysql_query("SELECT * FROM logs_anticheater WHERE usuario = '".base64_decode($_SESSION['us'])."' ORDER BY id DESC");
							if(mysql_num_rows($query) == 0)
							{
								echo '<p><div class="alert bg-primary" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> Você ainda não participou de nenhuma partida!<a href="#" class="pull-right"><em class="fa fa-lg fa-close"></em></a></div></p>';
							} else {
								echo '
								<table class="table table-hover table-sm">
									<thead>
										<tr>
											<th scope="col">#</th>
											<th scope="col">Partida</th>
											<th scope="col">Jogo</th>
											<th scope="col">Duração</th>
										</tr>
									</thead>
									<tbody>
									';
									$id = 1;
									$aux = null;
									while($res = mysql_fetch_array($query))
									{
										if($res['partida'] != $aux)
										{
											if($res['saiu'] != 0)
											{
												$sbsData1 = str_replace("/", "-", $res['entrou']);
												$sepData1 = explode("-", $sbsData1);
												$gambiarra1 = str_replace("2018", "", $sepData1[2]);
												$timeNovo1 = "2018"."-".$sepData1[1]."-".$sepData1[0].$gambiarra1;
					
												$sbsData2 = str_replace("/", "-", $res['saiu']);
												$sepData2 = explode("-", $sbsData2);
												$gambiarra2 = str_replace("2018", "", $sepData2[2]);
												$timeNovo2 = "2018"."-".$sepData2[1]."-".$sepData2[0].$gambiarra2;

												$date1 = $timeNovo1;
												$date2 = $timeNovo2;

												$dateS1 = new \DateTime($date1);
												$dateS2 = new \DateTime($date2);

												$dateDiff = $dateS1->diff($dateS2);
												$result = $dateDiff->h . ' horas e ' . $dateDiff->i . ' minutos';
											}

											$puxarJogo = mysql_query("SELECT * FROM partidas WHERE math = '".$res['partida']."' ORDER BY id DESC");
											$pj = mysql_fetch_array($puxarJogo);

											echo '
											<tr>
												<td>'.$id.'</td>
												<td>'.$res['partida'].'</td>
												<td>'.$pj['jogo'].'</td>
												<td>'.$result.'</td>
											</tr>';
											$id++;
										}
										$aux = $res['partida'];
									}
									echo '
									</tbody>
								</table>
								';
							}
						}
						echo '
					</div>
				</div>
			</div>
			';
		}

		if(isset($_GET['p']) && $_GET['p'] == base64_encode('st'))
		{
			if($res['permissao'] == 1)
			{
				echo '
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<span style="float:left;">Suporte técnico</span> 
							<a href="javascript:void(0)" onclick="window.history.go(-1)" class="btn btn-md btn-info" style="float:right;">Voltar</a>
						</div>
						<div class="panel-body">
							<iframe class="embed-responsive-item" src="chat/admin.php" style="width:100%; height:600px; border:none;"></iframe>
						</div>
					</div>
				</div>
				';
			}
		}

		if(isset($_GET['p']) && $_GET['p'] == base64_encode('software'))
		{
			if($res['permissao'] == 1)
			{
				$query = mysql_query("SELECT * FROM gerenciamento;");
				$res = mysql_fetch_array($query);
				echo '
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<span style="float:left;">Gerenciamento software</span> 
						</div>
						<div class="panel-body">
							<div style="margin:0 auto;">
							';
							if($res['status'] == 'true')
							{
								echo '<a href="index.php?ativarmanutencao" class="btn btn-success btn-lg btn-block" style="float:right;">Ativar manutenção</a>';
							} else {
								echo '<a href="index.php?desativarmanutencao" class="btn btn-danger btn-lg btn-block" style="float:right;">Desativar manutenção</a>';
							}
							if($res['login'] == 'true')
							{
								echo '<a href="index.php?desativarlogin" class="btn btn-warning btn-lg btn-block" style="float:right;">Desativar login</a>';
							} else {
								echo '<a href="index.php?ativarlogin" class="btn btn-warning btn-lg btn-block" style="float:right;">Ativar login</a>';
							}
							echo '
							</div>
						</div>
					</div>
				</div>
				';
			}
		}

		if(isset($_GET['p']) && $_GET['p'] == base64_encode('logs'))
		{
			if($res['permissao'] == 1)
			{
				if($_GET['id'] == 0) { 
					$inst = "SELECT * FROM logs ORDER BY id DESC;";
				}
				if($_GET['id'] == 1) { 
					$inst = "SELECT * FROM logs WHERE texto LIKE '%criou uma nova conta%' ORDER BY id DESC LIMIT 40;";
				}
				if($_GET['id'] == 2) { 
					$inst = "SELECT * FROM logs WHERE texto LIKE '%criou a partida%' ORDER BY id DESC LIMIT 40;";
				}
				if($_GET['id'] == 3) { 
					$inst = "SELECT * FROM logs WHERE texto LIKE '%errou a senha%' ORDER BY id DESC LIMIT 40;";
				}
				if($_GET['id'] == 4) { 
					$inst = "SELECT * FROM logs WHERE texto LIKE '%alterou%' ORDER BY id DESC LIMIT 40;";
				}
				$query = mysql_query($inst);
				
				echo '
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<span style="float:left; padding-right:10px;">Todos os logs </span> 
							<a href="/painel/auth_/logs/1" class="btn btn-default" style="margin-right:5px;">Contas criadas</a><a href="/painel/auth_/logs/2" class="btn btn-default" style="margin-right:5px;">Partidas criadas</a><a href="/painel/auth_/logs/3" class="btn btn-default" style="margin-right:5px;">Erros de senha</a><a href="/painel/auth_/logs/4" class="btn btn-default" style="margin-right:5px;">Alterações de dados</a><a href="/painel/auth_/logs/0" class="btn btn-default" style="margin-right:5px;">Todos</a>
						</div>
						<div class="panel-body">
							<div id="logs" style="background-color:#white; padding:10px; font-family:Calibri; font-size:18px; border-radius:5px; height:600px; overflow:auto;">
							';
							
							while($res = mysql_fetch_array($query))
							{
								echo "[".$res['data']." - ".$res['hora']."] ".$res['texto']."<br>";
							}
							echo '
							</div>
						</div>
					</div>
				</div>
				';
			}
		}

		if(isset($_GET['p']) && $_GET['p'] == base64_encode('enviar_email'))
		{
			if($res['permissao'] == 1)
			{
				$query = mysql_query("SELECT * FROM usuarios ORDER BY id DESC;");
				
				echo '
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<span style="float:left;">Enviar e-mail para os usuários cadastrados</span> 
							<small class="form-text text-muted" style="font-size:16px; color:#FF0631; float:right;">'.mysql_num_rows($query).' usuários receberam este e-mail!</small>
						</div>
						<div class="panel-body">
							<form method="post">
								<div class="form-group">
									<label for="exampleInputEmail1">Assunto</label>
									<input type="text" class="form-control" name="assunto">
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">Sua mensagem</label>
									<textarea class="form-control" name="msg"></textarea>
								</div>
								<div style="clear"></div>
								<input type="hidden" class="btn btn-primary" name="enviar_email" value="Enviar">
								<input type="submit" class="btn btn-primary" name="enviar_email" value="Enviar">
							</form>
						</div>
					</div>
				</div>
				';
			}
		}

		if(isset($_GET['p']) && $_GET['p'] == base64_encode('vercomprovantes'))
		{
			echo '
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<span style="float:left;">Comprovantes de pagamento de '.base64_decode($_SESSION['us']).'</span> 
						<a href="javascript:void(0)" onclick="window.history.go(-1)" class="btn btn-md btn-info" style="float:right;">Voltar</a>
					</div>
					<div class="panel-body">
						<div class="container" style="width:auto; display:table;">
							<div class="card-deck mb-3 text-center">
								<div class="container">
									<div class="row justify-content-md-center">
									';

									$query = mysql_query("SELECT * FROM comprovantes WHERE usuario = '".base64_decode($_SESSION['us'])."';");
									while($res = mysql_fetch_array($query))
									{
										echo '
										<div class="col col-lg-4">
											<img src="comprovantes/'.base64_decode($_SESSION['us']).'/'.$res['id'].'.jpg" class="img-fluid" style="width:100%;">
											<span>'.$res['anotacao'].'</span>
										</div>
										';
									}
									echo '
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			';
		}

		if(isset($_GET['p']) && $_GET['p'] == base64_encode('meuperfil'))
		{
			$sql = mysql_query("SELECT * FROM usuarios WHERE usuario = '".base64_decode($_SESSION['us'])."'");
			$res = mysql_fetch_array($sql);

			echo '
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<span style="float:left;">Minha conta</span> 
						<a href="javascript:void(0)" onclick="window.history.go(-1)" class="btn btn-md btn-info" style="float:right;">Voltar</a>
					</div>
					<div class="panel-body">
						<form method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label for="exampleInputEmail1">Nome completo</label>
								<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="nome" value="'.$res['nome'].'">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Usuário</label>
								<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="usuario" value="'.$res['usuario'].'">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Senha</label>
								<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="senha" value="">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Nickname</label>
								<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="nickname" value="'.$res['nickname'].'">
							</div>
							<div class="form-group">
								<label for="exampleFormControlFile1">Sua foto</label>
								<input type="file" class="form-control-file" name="img">
							</div>
							<input type="hidden" class="btn btn-primary" name="salvar" value="Salvar alterações">
							<input type="submit" class="btn btn-primary" name="salvar" value="Salvar alterações">
						</form>
					</div>
				</div>
			</div>
			';
		}

		if(isset($_GET['p']) && $_GET['p'] == base64_encode('editar_usuario'))
		{
			if($res['permissao'] == 1)
			{
				$sql = mysql_query("SELECT * FROM usuarios WHERE id = '".$_GET['id']."'");
				$res = mysql_fetch_array($sql);

				echo '
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<span style="float:left;">Edição de usuário</span> 
							<a href="javascript:void(0)" onclick="window.history.go(-1)" class="btn btn-md btn-info" style="float:right;">Voltar</a>
						</div>
						<div class="panel-body">
							<form method="post">
								<div class="form-group">
									<label for="exampleInputEmail1">Nome completo</label>
									<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="nome" value="'.$res['nome'].'">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Usuário</label>
									<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="usuario" value="'.$res['usuario'].'" readonly>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Senha</label>
									<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="senha" value="">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Nickname</label>
									<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="nickname" value="'.$res['nickname'].'">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Moedas</label>
									<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="moedas" value="'.$res['moedas'].'">
								</div>
								<input type="hidden" class="btn btn-primary" name="salvar" value="Salvar alterações">
								<input type="submit" class="btn btn-primary" name="salvar" value="Salvar alterações">
							</form>
						</div>
					</div>
				</div>
				';
			}
		}

		if(isset($_GET['p']) && $_GET['p'] == base64_encode('setar_compra'))
		{
			if($res['permissao'] == 1)
			{
				$sql = mysql_query("SELECT * FROM usuarios WHERE usuario = '".base64_decode($_GET['u'])."'");
				$res = mysql_fetch_array($sql);

				echo '
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<span style="float:left;">Setar compra para '.$res['usuario'].'</span> 
							<a href="javascript:void(0)" onclick="window.history.go(-1)" class="btn btn-md btn-info" style="float:right;">Voltar</a>
						</div>
						<div class="panel-body">
							<form method="post">
								<div class="form-group">
									<label for="exampleInputEmail1">Tipo de plano</label>
									<select class="form-control" name="planos">
										<option value="BASIC;15,99;">BASIC : R$ 15,99 / 20 partidas</option>
										<option value="PLUS;25,99;">PLUS : R$ 25,99 / 30 partidas</option>
										<option value="MAX;45,99;">MAX : R$ 45,99 / 100 partidas</option>
										<option value="TIER;95,99;">TIER : R$ 95,99 / 30 dias</option>
									</select>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Tipo de pagamento</label>
									<select class="form-control" name="tipo">
										<option value="Depósito bancário">Depósito bancário</option>
										<option value="Transferência bancária">Transferência bancária</option>
										<option value="Pagamento online">Pagamento online</option>
									</select>
								</div>
								<input type="hidden" class="btn btn-primary" name="salvar" value="Salvar alterações">
								<input type="submit" class="btn btn-primary" name="salvar" value="Salvar alterações">
							</form>
						</div>
					</div>
				</div>
				';
			}
		}

		if(isset($_GET['p']) && $_GET['p'] == base64_encode('ban'))
		{
			$sql = mysql_query("SELECT * FROM usuarios WHERE id = '".$_GET['id']."'");
			$res = mysql_fetch_array($sql);

			echo '
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<span style="float:left;">Banimento do jogador '.$_GET['nickname'].' - '.$_GET['usuario'].'</span> 
						<a href="javascript:void(0)" onclick="window.history.go(-1)" class="btn btn-md btn-info" style="float:right;">Voltar</a>
					</div>
					<div class="panel-body">
						<form method="post">
							<div class="form-group">
								<label for="exampleInputEmail1">Grade</label>
								<select class="form-control" name="motivo">
									<option value="Grade A">Grade A: Desrespeito, calúnia, difamação, racismo etc... (Banimento de 3 meses)</option>
									<option value="Grade B">Grade B: Utilização de meios e métodos para obter vantagem (Banimento de 6 meses)</option>
									<option value="Grade C">Grade C: Desrespeito às regras e/ou membros da organização do evento (Banimento de 1 mês)(</option>
									<option value="Grade E">Grade E: Tentativa de violação do software. (Banimento permanente)</option>
									<option value="Grade X">Grade X: Utilização de programas ilegais (Banimento permanente)</option>
								</select>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Jogo</label>
								<select class="form-control form-control-lg" name="jogo">
									<option selected disabled>Selecione</option>
									<option value="PointBlank">Point Blank</option>
									<option value="Zula">Zula</option>
									<option value="CrossFire">Cross Fire</option>
									<option value="BloodStrike">BloodStrike</option>
									<option value="BlackSquad">Black Squad</option>
									<option value="CombatArms">Combat Arms</option>
									<option value="LeagueOfLegends" disabled>League Of Legends (Em atualização)</option>
									<option value="PUBG">PUBG</option>
									<option value="KnivesOut">Knives Out</option>
									<option value="Warface">Warface</option>
									<option value="Todos">Todos</option>
								</select>
							</div>
							<input type="hidden" class="btn btn-primary" name="salvar" value="Salvar alterações">
							<input type="submit" class="btn btn-primary" name="salvar" value="Salvar alterações">
						</form>
					</div>
				</div>
			</div>
			';
		}
    ?>

	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>

	<script>
	$( function() {
		$("#tabs").tabs();
	});
	</script>

	
	<script>
	$( function() {
		$( "#accordion" ).accordion();
	} );
	</script>
	<script>
	window.onload = function () {
		var chart1 = document.getElementById("line-chart").getContext("2d");
		window.myLine = new Chart(chart1).Line(lineChartData, {
		responsive: true,
		scaleLineColor: "rgba(0,0,0,.2)",
		scaleGridLineColor: "rgba(0,0,0,.05)",
		scaleFontColor: "#c5c7cc"
		});
	};
	</script>

	
	<script>
	$(document).on('click', '#btn_editarpartida', function(){
		window.location.href = 'index.php?p=<?php echo base64_encode('editar_partida'); ?>&id='+$("#id_partida").attr("name");
	});
	</script>

	<script>

	/*$('#historico').on('click',function(e)
	{
		$('#home-tab').click();
	});*/

	$( function() {
		$('#home-tab').click();
	});

	$('#home-tab').on('click',function(e)
	{
		$('#home').show('fast');
		$('#profile').hide('fast');
	});
	$('#profile-tab').on('click',function(e)
	{
		$('#home').hide('fast');
		$('#profile').show('fast');
	});

	$('#nSalas').on('click',function(e)
	{
		$("#txtnSalas").text($("#nSalas").val() + " salas");
		$("#salas_criadas").text("");
		for(i = 0; i <$("#nSalas").val(); i++)
		{
			$("#salas_criadas").append('<li class="list-group-item d-flex justify-content-between align-items-center"><input type="text" style="background-color:white; border:none; font-family:Calibri; font-size:18px;" name="salas[]" value="ib_'+ Math.floor(Math.random() * 2001 + 6595 + $("#nSalas").val()) +'"> <span class="btn btn-default btn-sm"><input type="text" name="senhas[]" style="background-color:#E9ECF2; border:none; font-size:18px; font-family:Calibri; width:auto; display:auto;" value="'+Math.floor(Math.random() * 2001 + 6595 + $("#nSalas").val())+'"></span><span class="badge badge-default badge-pill"><i class="far fa-copy"></i> Copiar esta sala as salas</span><span class="badge badge-default badge-pill"><i class="far fa-copy"></i> Copiar todas</span> </li>');
		}
	});

	
	$('#copiarsalas').on('click',function(e)
	{
		var copyText = document.getElementById("salaray");
		copyText.select();
		document.execCommand("copy");
	});

	$('#deletarpartida').on('click',function(e)
	{
		$.ajax(
		{
			url: 'deletarPartida.php',
			type: 'post',
			data: {nome: $('#id_deletarpartida').attr('name')},
			success: function() { window.location.href='index.php?p=<?php echo base64_encode('partidas'); ?>'; }
		});
	});
	</script>

</body>
</html>