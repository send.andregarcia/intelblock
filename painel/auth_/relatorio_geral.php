<?php

date_default_timezone_set('America/Sao_Paulo');
session_start(); // IMPORTANTE: ao final dos comandos acima

if(!isset($_SESSION['us']))
{
	echo '<script language="JavaScript">window.location="../index.php";</script>';
} 

include("../conn.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="stylesheet" href="css/jquery-ui.css">

    <script>
        lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
        })
    </script>
    
    <style>
    .half {
        width: 100%;
    }
    .tab {
        position: relative;
        overflow: hidden;
    }
    .tab input {
        position: absolute;
        opacity: 0;
        z-index: -1;
    }
    .tab label {
        position: relative;
        display: block;
        padding: 0 0 0 1em;
        background: #333;
        line-height: 3;
        width: 100%;
        cursor: pointer;
        color: #fff;
    }
    .blue label {
        background: #2980b9;
    }
    .tab-content {
    
        overflow: hidden;
        background: #fff;
        -webkit-transition: max-height .35s;
        -o-transition: max-height .35s;
        transition: max-height .35s;
        margin-top:-7px;
        border:1px solid #ccc;
    }
    .blue .tab-content {
        background: #333;
    }
    .tab-content p {
    }
    /* :checked */
    .tab input:checked ~ .tab-content {
        max-height: 100vh;
    }
    /* Icon */
    .tab label::after {
        position: absolute;
        right: 0;
        top: 0;
        display: block;
        width: 3em;
        height: 3em;
        line-height: 3;
        text-align: center;
        -webkit-transition: all .35s;
        -o-transition: all .35s;
        transition: all .35s;
    }
    .tab input[type=checkbox] + label::after {
        content: "+";
    }
    .tab input[type=radio] + label::after {
        content: "\25BC";
    }
    .tab input[type=checkbox]:checked + label::after {
        transform: rotate(315deg);
    }
    .tab input[type=radio]:checked + label::after {
        transform: rotateX(180deg);
    }
    #screenshots img { 
        filter: brightness(0.40);
        border-radius:10px;
    }

    #screenshots img:hover { 
        filter: brightness(1);
        transition-delay: 0.1s;
    }
    </style>
    <title>Relatório geral</title>
</head>

<body style="background-color:#333;">
    <div class="container" style="background-color:white; padding:20px; margin-top:15px; border-radius:10px; width:50%; height:auto; display:table;">
        <img class="img-fluid rounded mx-auto d-block" src="../images/logoteste.png" style="width:30%;">
        <hr>
        <?php
            echo '
            <small id="emailHelp" class="form-text text-muted">Este relatório foi gerado pelo 
                <span style="color:#FF0631; font-weight:bold;">IntelBlock</span> às '.date("d/m/Y").' - '.date("H:i:m").' <span style="float:right;">
                    ';
                    if(isset($_GET['exibir']))
                    {
                        echo '<a href="relatorio_geral.php?sl='.$_GET['sl'].'&ss='.$_GET['ss'].'" id="voltar" style="margin-right:10px; color:#FF0631;"><i class="fas fa-chevron-left"></i> Voltar</a>';
                    }
                    echo '
                    <a href="javascript:void(0)" onclick="window.print();return false;" style="margin-right:10px;"><i class="fas fa-print"></i> Imprimir</a>
                </span>
            </small>';
        ?>
        <hr>

        <?php if(!isset($_GET['exibir'])): ?>
        <div id="parte1">
            <table class="table table-hover table-sm">
                <?php
                $i = 1;
                $exibiu = 0;
                $sql = mysql_query("SELECT * FROM logs_anticheater WHERE sala = '".base64_decode($_GET['sl'])."' ORDER BY id DESC");
                while($res = mysql_fetch_array($sql))
                {
                    if(!in_array($res['jogador'], $array))
                    {
                        $outraConsulta = mysql_query("SELECT * FROM logs_anticheater WHERE sala = '".base64_decode($_GET['sl'])."' AND jogador = '".$res['jogador']."' ORDER BY id DESC"); 
                        $resOutra = mysql_fetch_array($outraConsulta);
                        
                        if(mysql_num_rows($outraConsulta) != 0)
                        {
                            if($exibiu == 0)
                            {
                                echo '
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Jogador(a)</th>
                                        <th scope="col">Entrou em</th>
                                        <th scope="col">Saiu</th>
                                        <th scope="col">Permaneceu por</th>
                                        <th scope="col">Opções</th>
                                    </tr>
                                </thead>
                                <tbody>
                                ';
                                $exibiu = 1;
                            }
                            if($resOutra['saiu'] == 0)
                            {
                                $saiu = '<img src="images/online.gif">';
                            } else {
                                $saiu = $resOutra['saiu'];
                            }
                            
                            $sbsData1 = str_replace("/", "-", $resOutra['entrou']);
                            $sepData1 = explode("-", $sbsData1);
                            $gambiarra1 = str_replace("2018", "", $sepData1[2]);
                            $timeNovo1 = "2018"."-".$sepData1[1]."-".$sepData1[0].$gambiarra1;

                            $sbsData2 = str_replace("/", "-", $resOutra['saiu']);
                            $sepData2 = explode("-", $sbsData2);
                            $gambiarra2 = str_replace("2018", "", $sepData2[2]);
                            $timeNovo2 = "2018"."-".$sepData2[1]."-".$sepData2[0].$gambiarra2;

                            echo '
                            <tr>
                                <th scope="row">'.$i.'</th>
                                <td>'.$resOutra['jogador'].'</td>
                                <td>'.$resOutra['entrou'].'</td>
                                <td>'.$saiu.'</td>
                                <td>';
                                    if($saiu != 0)
                                    {
                                        $date1 = $timeNovo1;
                                        $date2 = $timeNovo2;

                                        $dateS1 = new \DateTime($date1);
                                        $dateS2 = new \DateTime($date2);

                                        $dateDiff = $dateS1->diff($dateS2);
                                        $result = $dateDiff->h . ' horas e ' . $dateDiff->i . ' minutos';
                                        echo $result;
                                    }
                                    echo '
                                </td>
                                <td><a href="relatorio_geral.php?sl='.$_GET['sl'].'&ss='.$_GET['ss'].'&u='.base64_encode($resOutra['usuario']).'&aloc='.base64_encode($resOutra['alocamento']).'&exibir" style="color:#FF0631;" class="exibir_dados"><i class="fas fa-upload"></i> Exibir dados coletados</a></td>
                            </tr>
                            ';  
                        } else {
                            echo '<div class="alert alert-primary" role="alert" style="margin:15px;"> Não há histórico de jogadores nessa partida!</div>';
                        }
                        $i++;
                    }
                    $array[$i] = $resOutra['jogador'];
                }
                ?>

                </tbody>
            </table>
        </div>
        <?php endif; ?>
                
                
        <?php if(isset($_GET['exibir'])): ?>
        
        <div id="parte2">
            <div class="half">
                <div class="tab">
                    <input id="tab-3" type="checkbox" name="tabs">
                    <label for="tab-3"><i class="fas fa-tv"></i> Informações do computador</label>
                    <div class="tab-content">
                        <p>
                        <?php 
                        $sql = mysql_query("SELECT * FROM info_usuarios WHERE usuario = '".mysql_real_escape_string(base64_decode($_GET['u']))."' ORDER BY id DESC");
                        if(mysql_num_rows($sql) == 0)
                        {
                            echo '<div class="alert alert-primary" role="alert" style="margin:15px;"> Não há nenhuma informação!</div>';
                        } else {
                            $res = mysql_fetch_array($sql);

                            echo '
                            <ul class="list-group">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Processador
                                    <span class="badge badge-primary badge-pill" style="font-size:17px;">'.$res['processador'].'</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Placa-mãe
                                    <span class="badge badge-primary badge-pill" style="font-size:17px;">'.$res['placamae'].'</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    HD
                                    <span class="badge badge-primary badge-pill" style="font-size:17px;">'.$res['hd'].'</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Memória
                                    <span class="badge badge-primary badge-pill" style="font-size:17px;">'.$res['memoria'].'</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Sistema Operacional
                                    <span class="badge badge-primary badge-pill" style="font-size:17px;">'.$res['os'].'</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    IP
                                    <span class="badge badge-primary badge-pill" style="font-size:17px;">'.$res['ip'].'</span>
                                </li>
                            </ul>
                            ';
                        }
                        ?>
                        </p>
                    </div>
                </div>
                <div class="tab">
                    <input id="tab-one" type="checkbox" name="tabs">
                    <label for="tab-one"><i class="fas fa-bell"></i> Notificações</label>
                    <div class="tab-content">
                        <p>
                        <?php 
                        $sql = mysql_query("SELECT * FROM alertas WHERE sala = '".base64_decode($_GET['sl'])."' AND senha = '".base64_decode($_GET['ss'])."' AND usuario = '".mysql_real_escape_string(base64_decode($_GET['u']))."'");
                        if(mysql_num_rows($sql) == 0)
                        {
                            echo '<div class="alert alert-primary" role="alert" style="margin:15px;"> Não há nenhum histórico!</div>';
                        } else {
                            echo '
                            <div class="list-group" style="padding:10px;">';

                            $i = 1;
                            while($res = mysql_fetch_array($sql))
                            {
                                /*echo '
                                <tr>
                                    <th scope="row">'.$i.'</th>
                                    <td>'.$res['mensagem'].'</td>
                                    <td>'.$res['processo'].'</td>
                                    <td>'.$res['data'].'</td>
                                    <td>'.$res['hora'].'</td>
                                </tr>
                                ';*/
                                echo '
                                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">'.$res['mensagem'].'</h5>
                                    <small>'.$res['data'].' - '.$res['hora'].'</small>
                                    </div>
                                    
                                </a>
                                ';
                                $i++;
                            }
                            echo '</div>';
                        }
                        ?>
                        </p>
                    </div>
                </div>
                <div class="tab">
                    <input id="tab-three" type="checkbox" name="tabs">
                    <label for="tab-three"><i class="fas fa-cogs"></i> Histórico de janelas ativas</label>
                    <div class="tab-content">
                        <p>
                        <ul class="list-group" style="margin:15px; height: 200px; overflow:auto;">
                            <?php
                                $query = mysql_query("SELECT * FROM janelas WHERE sala = '".base64_decode($_GET['sl'])."' AND senha = '".base64_decode($_GET['ss'])."' AND usuario = '".mysql_real_escape_string(base64_decode($_GET['u']))."' ORDER BY id ASC");
                                if(mysql_num_rows($query) == 0)
                                {
                                    echo '<div class="alert alert-warning" role="alert" style="margin:15px;"> Presente na próxima atualização do software para 1.1.19</div>';
                                } else {
                                    $i = 0;
                                    while($res = mysql_fetch_array($query))
                                    {
                                        if(!in_array('['.$res['data'].' - '.$res['hora'].'] '.$res['nome'], $array))
                                        {
                                            echo '<li class="list-group-item d-flex justify-content-between align-items-center">['.$res['data'].' - '.$res['hora'].'] '.$res['nome'].'</li>';
                                        }
                                        $array[$i] = '['.$res['data'].' - '.$res['hora'].'] '.$res['nome'];
                                        $i++;
                                    }
                                }
                            ?>
                        </ul>
                        </p>
                    </div>
                </div>
                <div class="tab">
                    <input id="tab-two" type="checkbox" name="tabs">
                    <label for="tab-two"><i class="far fa-window-restore"></i> Histórico de navegação</label>
                    <div class="tab-content">
                        <p>
                        <?php 
                        $query = mysql_query("SELECT * FROM sites_visitados WHERE sala = '".base64_decode($_GET['sl'])."' AND usuario = '".mysql_real_escape_string(base64_decode($_GET['u']))."'");
                        if(mysql_num_rows($query) == 0)
                        {
                            echo '<div class="alert alert-primary" role="alert" style="margin:15px;"> Não há nenhum histórico!</div>';
                        } else {
                            echo '
                            <table class="table table-hover table-sm" style="margin:15px; font-size:15px;">
                                <thead>
                                    <tr>
                                        <th width="6">#</th>
                                        <th width="24">Endereço</th>
                                        <th width="24">Termo</th>
                                        <th width="14">Visitas</th>
                                    </tr>
                                </thead>
                                <tbody>
                                ';

                                $i = 1;
                                while($res = mysql_fetch_array($query))
                                {
                                    echo '
                                    <tr>
                                        <th>'.$i.'</th>
                                        <td style="width:10%;">'.$res['url'].'</td>
                                        <td>'.$res['titulo'].'</td>
                                        <td>'.$res['visitas'].'</td>
                                    </tr>
                                    ';
                                    $i++;
                                }
                                echo '
                                </tbody>
                            </table>';
                        }
                        ?>
                        </p>
                    </div>
                </div>
                <div class="tab">
                    <input id="tab-5" type="checkbox" name="tabs">
                    <label for="tab-5"><i class="fas fa-file-download"></i> Histórico de download</label>
                    <div class="tab-content">
                        <p>
                        <?php 
                        $sql = mysql_query("SELECT * FROM downloads WHERE sala = '".base64_decode($_GET['sl'])."' AND usuario = '".mysql_real_escape_string(base64_decode($_GET['u']))."'");
                        if(mysql_num_rows($sql) == 0)
                        {
                            echo '<div class="alert alert-primary" role="alert" style="margin:15px;"> Não há nenhum histórico!</div>';
                        } else {
                            echo '
                            <table class="table table-hover table-sm" style="margin:15px;">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nome do arquivo</th>
                                        <th>Download original de</th>
                                    </tr>
                                </thead>
                                <tbody>
                                ';

                                $i = 1;
                                while($res = mysql_fetch_array($sql))
                                {
                                    echo '
                                    <tr>
                                        <th>'.$i.'</th>
                                        <td>'.$res['arquivo'].'</td>
                                        <td>'.$res['site'].'</td>
                                    </tr>
                                    ';
                                    $i++;
                                }
                                echo '
                                </tbody>
                            </table>';
                        }
                        ?>
                        </p>
                    </div>
                </div>
                <div class="tab">
                    <input id="tab-three" type="checkbox" name="tabs">
                    <label for="tab-three"><i class="fas fa-cogs"></i> Processos</label>
                    <div class="tab-content">
                        <p>
                        <ul class="list-group" style="margin:15px; height: 300px; overflow:auto;">
                            <?php
                                $query = mysql_query("SELECT * FROM processos WHERE sala = '".base64_decode($_GET['sl'])."' AND senha = '".base64_decode($_GET['ss'])."' AND usuario = '".mysql_real_escape_string(base64_decode($_GET['u']))."'");
                                if(mysql_num_rows($query) == 0)
                                {
                                    echo '<div class="alert alert-primary" role="alert" style="margin:15px;"> Não há nenhum processo!</div>';
                                } else {
                                    while($res = mysql_fetch_array($query))
                                    {
                                        echo '<li class="list-group-item d-flex justify-content-between align-items-center">'.$res['processo'].'</li>';
                                    }
                                }
                            ?>
                        </ul>
                        </p>
                    </div>
                </div>
                <div class="tab">
                    <input id="tab-4" type="checkbox" name="tabs">
                    <label for="tab-4"><i class="fas fa-camera-retro"></i> Screenshots</label>
                    <div class="tab-content">
                        <p>
                            <div style="margin: 0 auto; margin-left:12px;" id="screenshots">
                            <?php
                                if(mysql_real_escape_string(base64_decode($_GET['aloc'])) == 0)
                                {
                                    $servidor = 20;
                                } else {
                                    $servidor = mysql_real_escape_string(base64_decode($_GET['aloc']));
                                }
                                //echo $servidor;
                                $CURL = curl_init();
                                $configCURL = array( CURLOPT_URL => 'http://198.50.198.40/_ups_/api.php?listar&u='.mysql_real_escape_string(base64_decode($_GET['u'])).'&sl='.mysql_real_escape_string(base64_decode($_GET['sl'])).'&ss='.mysql_real_escape_string(base64_decode($_GET['ss'])).'&aloc='.$servidor, CURLOPT_RETURNTRANSFER => true);  
                                curl_setopt_array($CURL, $configCURL); 
                                $resultado = curl_exec($CURL);  
                                $Erro = curl_error($CURL);  

                                //var_dump($Erro);

                                $id = 1;
                                $separar = explode(";", $resultado);
                                foreach($separar as $fotos)
                                {
                                    if($fotos != '' || $fotos != null)
                                    {
                                        if($id > 6)
                                        {
                                            $margin1 = "margin-top:30px;";
                                            $margin2 = "margin-top:5px;";
                                        } else {
                                            $margin1 = "margin-top:12px;";
                                            $margin2 = "margin-top:-13px;";
                                        }
                                        $separar_prm = explode("-", $fotos);
                                        echo '
                                        <a href="'.$fotos.'" data-lightbox="roadtrip">
                                            <img src="'.$fotos.'" style="width:137px; height:137px; margin:3px; '.$margin1.'">
                                            <span style="background-color:red; font-size:13px; padding:2px; border-radius:5px; color:white; position:absolute; text-align:center; margin-left:-134px; '.$margin2.' z-index:9999;">'.$separar_prm[3]."x".$separar_prm[4].' /'.$separar_prm[5].' bits</span>
                                        </a>
                                        ';
                                    }
                                    $id++;
                                }

                                /*$caminho = 'prints';

                                $fotos = glob("$caminho/{*}",GLOB_BRACE);
                                foreach ($fotos as $nomeFoto) 
                                {
                                    $arrayArquivos[date('Y/m/d H:i:s', filemtime($nomeFoto))] = $nomeFoto;
                                }
                                
                                ksort($arrayArquivos, SORT_STRING);

                                $fotos = glob("$caminho/{*}",GLOB_BRACE);
                                foreach ($arrayArquivos as $nomeFoto) 
                                {
                                    $replace = str_replace("prints/", "", $nomeFoto);
                                    $separar = explode("-", $replace);
                                    if($separar[0] == $_GET['u'] && $separar[1] == base64_decode($_GET['sl']) && $separar[2] == base64_decode($_GET['ss']))
                                    {
                                        echo '<a href="'.$nomeFoto.'" data-lightbox="roadtrip"><img src="'.$nomeFoto.'" style="width:101px; margin:3px;"></a>';
                                    }
                                }*/
                            ?>
                            </div>
                            
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>

    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <link href="css/lightbox.css" rel="stylesheet">
    <script src="js/lightbox.js"></script>
    
    <script>
    $( function() {
        $( "#accordion" ).accordion();
    } );
    </script>

    <script>
    $('.exibir_dados').on('click',function(e)
	{
        $('#parte1').hide('fast');
        $('#parte2').show('fast');
        $('#voltar').show('fast');
	});
    $('#voltar').on('click',function(e)
	{
        $('#parte2').hide('fast');
        $('#parte1').show('fast');
        $('#voltar').hide('fast');
	});
    </script>

</body>

</html>