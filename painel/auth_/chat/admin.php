<?php

include("../../conn.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="folha.css?rand=<?php echo rand(0, 999); ?>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <title>Administrador</title>
</head>
<body>
    
<div class="container">
  <div class="messaging">
    <div class="inbox_msg">
      <div class="inbox_people">
        <div class="headind_srch">
          <div class="recent_heading">
            <h4>Usuários</h4>
          </div>
          <!--
          <div class="srch_bar">
            <div class="stylish-input-group">
              <input type="text" class="search-bar"  placeholder="Search" >
              <span class="input-group-addon">
              <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
              </span> </div>
          </div>-->

        </div>
        <div class="inbox_chat">
          <?php
            $sql = pg_query("SELECT * FROM s_administradores.usuarios");
            while($res = pg_fetch_array($sql))
            {
              echo '
              <a href="javascript:void(0)" id="conversa" name="'.$res['usuario'].'">
                <div class="chat_list">
                  <div class="chat_people">
                    <div class="chat_img"><img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                    <div class="chat_ib"><h5 style="font-size:21px; line-height:36px;">'.$res['nome'].'</h5></div>
                  </div>
                </div>
              </a>
              ';
            }
          ?>
        </div>
      </div>
      <div class="mesgs" style="background-image:url(fundo.jpg); padding:5px;">
        <div class="msg_history" style="padding:15px;">
          <div id="carregar_mensagens"></div>
        </div>
        <div class="type_msg" style="background-color:white;">
          <div class="input_msg_write">
            <form method="post" id="formEnviarMsg"> 
              <input type="text" class="write_msg" name="mensagem" id="mensagem" style="padding:7px; border-radius:15px;" placeholder="Escreva sua mensagem" />
              <input type="hidden" name="recuperar_usuario" id="recuperar_usuario" value=""/>
              <input type="hidden" name="tipousuario" id="tipousuario" value="admin"/>
              <button class="msg_send_btn" type="submit" id="enviar_mensagem" style="top:9px; right:9px;"><i class="fas fa-paper-plane"></i></button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

  <script> 

  setInterval(function()
  { 
    $.post('atualizar_mensagens.php', {usuario: $('#recuperar_usuario').val() }, function(data)
    {
      if(data > $('.received_withd_msg').length)
      {
        atualizar_mensagens($('#recuperar_usuario').val());
      }
    });

  }, 300);

  function atualizar_mensagens(nome_usuario)
  {
    $.post('mensagens_admin.php', {usuario: nome_usuario }, function(data){
      $('#carregar_mensagens').html(data);
      $('.msg_history').scrollTop(20000);
    });
  }

  $(document).on('click', '#conversa', function(){
    //alert($(this).attr('name')); 
    //$('#trocar').text($(this).attr('name')); formEnviarMsg / enviar_mensagem
    $('#recuperar_usuario').val($(this).attr('name'));
    atualizar_mensagens($(this).attr('name'));
  });

  $(function()
  {
    $("#formEnviarMsg").submit(function(event) 
    {
      var formDados = new FormData($(this)[0]);

      $.ajax({
        url: 'inserir_mensagem.php',
        type: 'POST',
        data: formDados,
        cache: false,
        contentType: false,
        processData: false,
        sucess: function(data)
        {	 
        },
        dataType: 'html'
      });
      $('#mensagem').val('');
      atualizar_mensagens($('#recuperar_usuario').val());
      return false;
    });
  });
  $('#conversa').click();
  </script>

</body>
</html>