<?php

session_start();
include("../conn.php");

function upload($caminho, $nome, $files)
{
    if(!is_dir($caminho)) { mkdir($caminho, 0755, true); }
    $Arquivo 	 = $_FILES[$files];
    if(isset($Arquivo['name']))
    {
        $name   = $Arquivo['name']; $type   = $Arquivo['type'];
        $size   = $Arquivo['size']; $error  = $Arquivo['error'];
        $tmp    = $Arquivo['tmp_name']; $Extensao = @end(explode('.', $name));
        $novoNome = $nome.".jpg";
        $Subindo  = move_uploaded_file($tmp, $caminho.'/'.$novoNome);
    }
}

if(isset($_POST['criar_moderador']) && $_POST['criar_moderador'] == 'Adicionar moderador')
{
    $sql = mysql_query("SELECT * FROM usuarios WHERE usuario = '".$_POST['email']."'");
    if(mysql_num_rows($sql) == 0)
    {
        echo '<script>alert("Verifique se você digitou corretamente o e-mail do usuário")</script>';
    } else {
        $sql = mysql_query("SELECT * FROM moderadores WHERE usuario = '".$_POST['email']."' AND organizacao = '".base64_decode($_SESSION['us'])."'");
        if(mysql_num_rows($sql) == 0)
        {
            if(!isset($_POST['criar_partida']) && !isset($_POST['monitoramento'])) 
            { 
                echo '<script>alert("O moderador precisa de no mínimo uma permissão para ser criado")</script>';
            } else {
                if(isset($_POST['criar_partida'])) { 
                    $permissao = "criar partida.";
                }
                if(isset($_POST['monitoramento'])) { 
                    $permissao = "monitorar os jogadores.";
                }
                if(isset($_POST['criar_partida']) && isset($_POST['monitoramento'])) { 
                    $permissao = "criar partida e monitorar os jogadores.";
                }
                mysql_query("INSERT INTO moderadores (usuario, organizacao, criar_partida, ver_relatorio) VALUES ('".$_POST['email']."', '".base64_decode($_SESSION['us'])."', '".$_POST['criar_partida']."', '".$_POST['monitoramento']."')");
                mysql_query("INSERT INTO logs (texto, data, hora) VALUES ('Usuário ".base64_decode($_SESSION['us'])." setou como moderador o ".$_POST['email']." com permissões para ".$permissao."', '".date("d/m/Y")."', '".date("H:i:m")."');");
                header("Location: /painel/auth_/moderadores");
            }
        } else {
            echo '<script>alert("Este usuário já é moderador!")</script>';
        }
    }
}

if(isset($_POST['editar_moderador']) && $_POST['editar_moderador'] == 'Salvar')
{
    $sql = mysql_query("SELECT * FROM usuarios WHERE usuario = '".$_POST['email']."'");
    if(mysql_num_rows($sql) == 0)
    {
        echo '<script>alert("Verifique se você digitou corretamente o e-mail do usuário")</script>';
    } else {
        if(!isset($_POST['criar_partida']) && !isset($_POST['monitoramento'])) 
        { 
            echo '<script>alert("O moderador precisa de no mínimo uma permissão para ser criado")</script>';
        } else {
            if(isset($_POST['criar_partida'])) { 
                $permissao = "criar partida.";
            }
            if(isset($_POST['monitoramento'])) { 
                $permissao = "monitorar os jogadores.";
            }
            if(isset($_POST['criar_partida']) && isset($_POST['monitoramento'])) { 
                $permissao = "criar partida e monitorar os jogadores.";
            }
            mysql_query("UPDATE moderadores SET usuario = '".$_POST['email']."', organizacao = '".base64_decode($_SESSION['us'])."', criar_partida = '".$_POST['criar_partida']."', ver_relatorio = '".$_POST['monitoramento']."' WHERE id = '".$_GET['id']."'");
            mysql_query("INSERT INTO logs (texto, data, hora) VALUES ('Usuário ".base64_decode($_SESSION['us'])." alterou o moderador ".$_SESSION['modantigo']." para ".$_POST['email']." com as permissões para ".$permissao."', '".date("d/m/Y")."', '".date("H:i:m")."');");
            header("Location: /painel/auth_/moderadores");
        }
    }
}

if(isset($_GET['p']) && $_GET['p'] == base64_encode('apagar_moderador'))
{
    $sql = mysql_query("DELETE FROM moderadores WHERE id = '".$_GET['id']."'");
    $res = mysql_fetch_array($sql);

    mysql_query("INSERT INTO logs (texto, data, hora) VALUES ('Usuário ".base64_decode($_SESSION['us'])." apagou o moderador ".$res['usuario']."', '".date("d/m/Y")."', '".date("H:i:m")."');");
    mysql_query("DELETE FROM moderadores WHERE id = '".$_GET['id']."'");
    
    header("Location: /painel/auth_/moderadores");
}

if(isset($_POST['salvar']) && $_POST['salvar'] == 'Criar partida')
{
    if($_POST['jogo'] == '' || $_POST['jogo'] == 'Selecione')
    {
        echo '<script>alert("Você precisa selecionar um jogo para esta partida!")</script>';
    } 
    else if($_POST['qntSalas'] == 0) 
    {
        echo '<script>alert("Você precisa criar no mínimo uma sala para esta partida!")</script>';
    } else {
        if($_POST['jogo'] == 'PointBlank') { $processo = 'PointBlank'; }
        if($_POST['jogo'] == 'Zula') { $processo = 'zula'; }
        if($_POST['jogo'] == 'CrossFire') { $processo = 'crossfire'; }
        if($_POST['jogo'] == 'BloodStrike') { $processo = 'ChromiumCEF'; }
        if($_POST['jogo'] == 'BlackSquad') { $processo = 'BlackSquadGame'; }
        if($_POST['jogo'] == 'PUBG') { $processo = 'TslGame'; }
        if($_POST['jogo'] == 'KnivesOut') { $processo = 'hyxd'; }
        if($_POST['jogo'] == 'CombatArms') { $processo = 'Engine'; }
        if($_POST['jogo'] == 'Warface') { $processo = 'Game'; }

        $criador = $_SESSION['us'];
        if(isset($_GET['tp']) && $_GET['tp'] == 'usuario')
        {
            $criador = $_SESSION['us'];
            $mod = "Usuário ";
        } else {
            $criador = $_GET['tp'];
            $mod = "Moderador ";
        }

        $sql = mysql_query("SELECT * FROM usuarios WHERE usuario = '".base64_decode($criador)."'");
        $res = mysql_fetch_array($sql);

        if($res['moedas'] == 0 || $_POST['qntSalas'] > $res['moedas'])
        {
            echo '<script>alert("Você não tem moedas sulficientes para criar uma partida!")</script>';
            mysql_query("INSERT INTO logs (texto, data, hora) VALUES ('".$mod.base64_decode($_SESSION['us'])." tentou criar uma partida ".$nomePartida." com ".$i." salas | Moedas: ".$res['moedas']."', '".date("d/m/Y")."', '".date("H:i:m")."');");
        } else {

            $nomePartida = rand(0, 9999).date("H").strlen($_POST['nome']).";".$_POST['nome'];
            mysql_query("INSERT INTO partidas (math, criador, jogo, processo, data, tabela, moderador) VALUES ('".$nomePartida."', '".base64_decode($criador)."', '".$_POST['jogo']."', '".$processo."', '".date("d/m/Y")."', '".$_POST['tabela']."', '".base64_decode($_SESSION['us'])."');");

            $salasGeradas = 0;
            for($i = 0; $i < $_POST['qntSalas']; $i++)
            {
                $horario = date("H:i:m");
                $repHorario = str_replace(":", "", $horario);

                $sala = rand(1000, 99999).substr(0, 3, $repHorario);
                $senha = rand(10, 99).substr(0, 3, $repHorario).rand(1, 999);
                mysql_query("INSERT INTO salas (nome, senha, partida, criador) VALUES ('ib_".$sala."', '".$senha."', '".$nomePartida."', '".base64_decode($criador)."');");
                $salasGeradas++;
            }

            if($res['moedas'] != 9999) 
            { 
                mysql_query("UPDATE usuarios SET moedas = '".($res['moedas']-$_POST['qntSalas'])."' WHERE usuario = '".base64_decode($criador)."'");
            }

            mysql_query("INSERT INTO historico_partidas (partida, jogo, usuario, data, hora, salas) VALUES ('".$nomePartida."', '".$_POST['jogo']."', '".base64_decode($criador)."', '".date("d/m/Y")."', '".date("H:i:m")."', '".$salasGeradas."')") or die(Mysql_Error());

            mysql_query("INSERT INTO logs (texto, data, hora) VALUES ('".$mod.base64_decode($_SESSION['us'])." criou a partida ".$nomePartida." com ".$i." salas', '".date("d/m/Y")."', '".date("H:i:m")."');");
            
            if(isset($_GET['tp']) && $_GET['tp'] != 'usuario')
            {
                mysql_query("INSERT INTO historico_moderacao (moderador, jogo, texto, data, hora) VALUES ('".base64_decode($_SESSION['us'])."', '".$_POST['jogo']."', '".base64_decode($_SESSION['us'])." criou a partida ".$_POST['nome']." com ".$salasGeradas." salas', '".date("d/m/Y")."', '".date("H:i:m")."')") or die(Mysql_Error());
            }
        }
        
        header("Location: /painel/auth_/partidas/".$criador);
    }
}

if(isset($_POST['salvar']) && $_POST['salvar'] == 'Salvar alterações')
{
    if(isset($_GET['p']) && $_GET['p'] == base64_encode('partidas'))
    {
        $sql = mysql_query("SELECT * FROM partidas WHERE id = '".$_GET['id']."'");
        $res = mysql_fetch_array($sql);

        mysql_query("UPDATE salas SET partida = '".mysql_real_escape_string($_POST['nome'])."' WHERE partida = '".$res['math']."'");
        mysql_query("UPDATE partidas SET math = '".mysql_real_escape_string($_POST['nome'])."' WHERE id = '".$_GET['id']."'");

        header("Location: index.php?p=".base64_encode('partidas'));
    }
    if(isset($_GET['p']) && $_GET['p'] == base64_encode('editar_usuario'))
    {
        $sql = mysql_query("SELECT * FROM usuarios WHERE id = '".$_GET['id']."'");
        $res = mysql_fetch_array($sql);

        mysql_query("UPDATE usuarios SET nome = '".mysql_real_escape_string($_POST['nome'])."', nickname = '".mysql_real_escape_string($_POST['nickname'])."', moedas = '".mysql_real_escape_string($_POST['moedas'])."' WHERE id = '".$_GET['id']."';");
        if($_POST['senha'] != '')
        {
            mysql_query("UPDATE usuarios SET senha = '".mysql_real_escape_string(md5($_POST['senha']))."' WHERE id = '".$_GET['id']."';");
        } 

        mysql_query("INSERT INTO logs (texto, data, hora) VALUES ('Usuário ".mysql_real_escape_string($_POST['nome'])." foi alterado. Moedas: '".$_POST['moedas'].", '".date("d/m/Y")."', '".date("H:i:m")."');");
        
        header("Location: index.php?p=".base64_encode('gu'));
    }
    if(isset($_GET['p']) && $_GET['p'] == base64_encode('meuperfil'))
    {
        $sql = mysql_query("SELECT * FROM usuarios WHERE usuario = '".base64_decode($_SESSION['us'])."'");
        $res = mysql_fetch_array($sql);

        if($_POST['senha'] != '')
        {
            mysql_query("UPDATE usuarios SET nome = '".mysql_real_escape_string($_POST['nome'])."', senha = '".mysql_real_escape_string(md5($_POST['senha']))."' WHERE usuario = '".base64_decode($_SESSION['us'])."';");
        } else {
            mysql_query("UPDATE usuarios SET nome = '".mysql_real_escape_string($_POST['nome'])."' WHERE usuario = '".base64_decode($_SESSION['us'])."';");
        }

        if(base64_decode($_SESSION['us']) != $_POST['usuario'])
        {
            mysql_query("UPDATE alertas SET usuario = '".mysql_real_escape_string($_POST['usuario'])."' WHERE usuario = '".base64_decode($_SESSION['us'])."';");
            mysql_query("UPDATE comprovantes SET usuario = '".mysql_real_escape_string($_POST['usuario'])."' WHERE usuario = '".base64_decode($_SESSION['us'])."';");
            mysql_query("UPDATE downloads SET usuario = '".mysql_real_escape_string($_POST['usuario'])."' WHERE usuario = '".base64_decode($_SESSION['us'])."';");
            mysql_query("UPDATE historico_compras SET usuario = '".mysql_real_escape_string($_POST['usuario'])."' WHERE usuario = '".base64_decode($_SESSION['us'])."';");
            mysql_query("UPDATE historico_partidas SET usuario = '".mysql_real_escape_string(base64_encode($_POST['usuario']))."' WHERE usuario = '".base64_decode($_SESSION['us'])."';");
            mysql_query("UPDATE info_usuarios SET usuario = '".mysql_real_escape_string($_POST['usuario'])."' WHERE usuario = '".base64_decode($_SESSION['us'])."';");
            mysql_query("UPDATE logs_anticheater SET usuario = '".mysql_real_escape_string($_POST['usuario'])."' WHERE usuario = '".base64_decode($_SESSION['us'])."';");
            mysql_query("UPDATE partidas SET criador = '".mysql_real_escape_string(base64_encode($_POST['usuario']))."' WHERE usuario = '".$_SESSION['us']."';");
            mysql_query("UPDATE processos SET usuario = '".mysql_real_escape_string($_POST['usuario'])."' WHERE usuario = '".base64_decode($_SESSION['us'])."';");
            mysql_query("UPDATE salas SET criador = '".mysql_real_escape_string(base64_encode($_POST['usuario']))."' WHERE criador = '".$_SESSION['us']."';");
            mysql_query("UPDATE sites_visitados SET usuario = '".mysql_real_escape_string($_POST['usuario'])."' WHERE usuario = '".base64_decode($_SESSION['us'])."';");
            mysql_query("UPDATE usuarios SET usuario = '".mysql_real_escape_string($_POST['usuario'])."' WHERE usuario = '".base64_decode($_SESSION['us'])."';");
            mysql_query("INSERT INTO logs (texto, data, hora) VALUES ('".base64_decode($_SESSION['us'])." alterou o usuário para ".$_POST['usuario']."', '".date("d/m/Y")."', '".date("H:i:m")."');");

            $CURL = curl_init();
            $configCURL = array( CURLOPT_URL => 'http://websitehosting.com.br/_ups_/api.php?editar&u='.base64_decode($_SESSION['us']).'&novo='.$_POST['usuario'], CURLOPT_RETURNTRANSFER => true);  
            curl_setopt_array($CURL, $configCURL); $resultado = curl_exec($CURL);  $Erro = curl_error($CURL);  

            rename("og/".$_SESSION['us'].".jpg", "og/".base64_encode($_POST['usuario']).".jpg");

            $_SESSION['us'] = base64_encode($_POST['usuario']);
        }

        if(base64_decode($_SESSION['nck']) != $_POST['nickname'])
        {
            mysql_query("UPDATE alertas SET nickname = '".mysql_real_escape_string($_POST['nickname'])."' WHERE nickname = '".base64_decode($_SESSION['nck'])."';");
            mysql_query("UPDATE historico_compras SET nickname = '".mysql_real_escape_string($_POST['nickname'])."' WHERE nickname = '".base64_decode($_SESSION['nck'])."';");
            mysql_query("UPDATE info_usuarios SET nickname = '".mysql_real_escape_string($_POST['nickname'])."' WHERE nickname = '".base64_decode($_SESSION['nck'])."';");
            mysql_query("UPDATE logs_anticheater SET jogador = '".mysql_real_escape_string($_POST['nickname'])."' WHERE jogador = '".base64_decode($_SESSION['nck'])."';");
            mysql_query("UPDATE usuarios SET nickname = '".mysql_real_escape_string($_POST['nickname'])."' WHERE nickname = '".base64_decode($_SESSION['nck'])."';");
            mysql_query("INSERT INTO logs (texto) VALUES ('".base64_decode($_SESSION['nck'])." alterou o nickname para ".$_POST['nickname']."');");
            $_SESSION['nck'] = base64_encode($_POST['nickname']);
        }
        upload("og/", $_SESSION['us'], "img");

        header("Location: /painel/auth_/perfil");
    }
    if(isset($_GET['p']) && $_GET['p'] == base64_encode('ban'))
    {
        $separar = explode("|", $_POST['jogador']);

        $sql = mysql_query("SELECT * FROM info_usuarios WHERE usuario = '".$_GET['usuario']."' ORDER BY id DESC");
        $res = mysql_fetch_array($sql);


        $mesesBan = 0;
        
        if($_POST['motivo'] == 'Grade A') { 

            if((date("m") + 3) > 12) 
            {
                $ano = 2019;
                $mesesBan = 3;
                $dif = (12 - date("m"));
                $calc = ((date("m") + 3) - date("m")) - $dif;
                $desban = date("d")."/0".$calc."/".$ano;
            } else {
                $ano = 2018;
                $desban = date("d")."/".(date("m") + 3)."/".$ano;
            }
        }
        if($_POST['motivo'] == 'Grade B') {
            if((date("m") + 6) > 12) 
            {
                $ano = 2019;
                $mesesBan = 6;
                $dif = (12 - date("m"));
                $calc = ((date("m") + 6) - date("m")) - $dif;
                $desban = date("d")."/0".$calc."/".$ano;
            } else {
                $ano = 2018;
                $desban = date("d")."/".(date("m") + 6)."/".$ano;
            }
        }
        if($_POST['motivo'] == 'Grade C') { 
            if((date("m") + 1) > 12) 
            {
                $ano = 2019;
                $mesesBan = 1;
                $dif = (12 - date("m"));
                $calc = ((date("m") + 1) - date("m")) - $dif;
                $desban = date("d")."/0".$calc."/".$ano;
            } else {
                $ano = 2018;
                $desban = date("d")."/".(date("m") + 1)."/".$ano;
            }
        }
        if($_POST['motivo'] == 'Grade E') { 
            $desban = date("d")."/".date("m")."/2050";
        }
        if($_POST['motivo'] == 'Grade X') { 
            $desban = date("d")."/".date("m")."/2050";
        }

        mysql_query("INSERT INTO bans (ip, mac, usuario, nickname, banimento, desbanimento, motivo, jogo) VALUES ('".$res['ip']."', '".$res['mac']."', '".$_GET['usuario']."', '".$_GET['nickname']."', '".date("d/m/Y")."', '".$desban."', '".$_POST['motivo']."', '".$_POST['jogo']."');");

        header("Location: index.php?p=".base64_encode('gu'));
    }
    if(isset($_GET['p']) && $_GET['p'] == base64_encode('setar_compra'))
    {
        $sql = mysql_query("SELECT * FROM usuarios WHERE usuario = '".base64_decode($_GET['u'])."'");
        $res = mysql_fetch_array($sql);

        $separar = explode(";", $_POST['planos']);
        
        if($separar[0] == "BASIC")
        {
            $moedas = 20;
        }
        else if($separar[0] == "PLUS")
        {
            $moedas = 30;
        }
        else if($separar[0] == "MAX")
        {
            $moedas = 100;
        }
        else if($separar[0] == "TIER")
        {
            $moedas = 9999;
        }

        if($separar[0] == "TIER")
        {
            mysql_query("INSERT INTO historico_compras (plano, valor, tipo_pagamento, data, hora, usuario, nickname, vencimento) VALUES ('".$separar[0]."', '".$separar[1]."', '".$_POST['tipo']."', '".date("d/m/Y")."', '".date("H:i:m")."', '".$res['usuario']."', '".$res['nickname']."', '".date("d")."/".(date("m")+1)."/".date("y")."');");
        } else {
            mysql_query("INSERT INTO historico_compras (plano, valor, tipo_pagamento, data, hora, usuario, nickname) VALUES ('".$separar[0]."', '".$separar[1]."', '".$_POST['tipo']."', '".date("d/m/Y")."', '".date("H:i:m")."', '".$res['usuario']."', '".$res['nickname']."');");
        }

        mysql_query("UPDATE usuarios SET moedas = '".$moedas."' WHERE usuario = '".$res['usuario']."';");
        

        header("Location: index.php?p=".base64_encode('gu'));
    }
}

if(isset($_GET['p']) && $_GET['p'] == base64_encode('desban'))
{
    mysql_query("DELETE FROM bans WHERE nickname = '".$_GET['nickname']."' AND usuario = '".$_GET['usuario']."';");
    header("Location: index.php?p=".base64_encode('gu'));
}

if(isset($_POST['salvar']) && $_POST['salvar'] == 'Enviar comprovante')
{
    mysql_query("INSERT INTO comprovantes (anotacao, usuario, data) VALUES ('".mysql_real_escape_string($_POST['anotacao'])."', '".$_SESSION['us']."', '".date("d/m/Y H:i:m")."');");

    $capturarID = mysql_query("SELECT id,usuario FROM comprovantes WHERE usuario = '".$_SESSION['us']."' ORDER BY id DESC"); 
    $resID = mysql_fetch_array($capturarID);

    upload("comprovantes/".$_SESSION['us'], $resID['id'], "img");
    header("Location: index.php?p=".base64_encode('planos'));
}

if(isset($_POST['enviar_email']) && $_POST['enviar_email'] == 'Enviar')
{
    $query = mysql_query("SELECT * FROM usuarios ORDER BY id DESC;");
    while($res = mysql_fetch_array($query))
    {
        $msg = str_replace("{u}", $res['nickname'], $_POST['msg']);
        $CURL = curl_init();
        $configCURL = array(CURLOPT_URL => 'http://grupoinventa.com.br/api/api-email.php?newsletter&email='.$res['usuario'].'&assunto='.base64_encode($_POST['assunto']).'&msg='.base64_encode($msg), CURLOPT_RETURNTRANSFER => true);  
        curl_setopt_array($CURL, $configCURL);
        $Resultado = curl_exec($CURL);  
        $Erro = curl_error($CURL);
        curl_close($CURL);
        sleep(1);
    }
}

if(isset($_GET['p']) && $_GET['p'] == base64_encode('confirmarconta'))
{
    mysql_query("UPDATE usuarios SET status = '1' WHERE id = '".$_GET['id']."';");
    header("Location: /painel/auth_/gerenciar/1/1");
}

?>