<?php

/*ini_set('session.gc_maxlifetime', ($timeout * 60)); 
ini_set('session.use_strict_mode', true); // aceitar apenas sessões criadas pelo módulo session
ini_set('session.use_cookies', true); // usar junto com use_only_cookies
ini_set('session.use_only_cookies', true); // cookies gerados apenas pelo proprio usuário
ini_set('session.cookie_httponly', true); // cookies só acessíveis por HTTP (não JS)
ini_set('session.cookie_secure', false); // cookies só acessíveis por HTTPS
ini_set('session.hash_function', 'sha512'); // criptografa session: dificulta Session Hijacking       
ini_set('session.use_trans_sid', false); // suporte a SID transparente desabilitado
ini_set('session.referer_check', 'http://www.intelblock.com.br'); // checa o referer
ini_set('session.cache_limiter', 'nocache'); // não fazer cache
session_regenerate_id(); // renova ID da seção*/
date_default_timezone_set('America/Sao_Paulo');
session_start(); // IMPORTANTE: ao final dos comandos acima

include('conn.php');

$pg = mysql_query("SELECT * FROM gerenciamento");
$res = mysql_fetch_array($pg);
if($res['status'] == 'false')
{
    header("Location: ../_404/");
}

if(isset($_POST['entrar']) && $res['login'] == 'true')
{
	$verificar_ban = mysql_query("SELECT * FROM bans WHERE usuario = '".mysql_real_escape_string($_POST['usuario'])."' OR ip = '".$_SERVER['REMOTE_ADDR']."'");
	if(mysql_num_rows($verificar_ban))
	{
		$_SESSION['b'] = mysql_real_escape_string($_POST['usuario']);
		$_SESSION['i'] = $_SERVER['REMOTE_ADDR'];
		header("Location: banido.php");
	} else {
		$sql1 = mysql_query("SELECT * FROM usuarios WHERE usuario = '".mysql_real_escape_string($_POST['usuario'])."' AND senha = '".md5(mysql_real_escape_string($_POST['senha']))."'");
		if(mysql_num_rows($sql1))
		{
			$res = mysql_fetch_array($sql1);
			if($res['status'] == 0)
			{
				echo '<script>alert("Você precisa confirmar seu cadastro. Verifique seu e-mail!");</script>';
			} else {
				$_SESSION['us'] = base64_encode($_POST['usuario']);
				$_SESSION['nck'] = base64_encode($res['nickname']);
				$_SESSION['v'] = date("d");
				mysql_query("INSERT INTO logs (texto, data, hora) VALUES ('Logou no painel | Usuário: ".mysql_real_escape_string($_POST['usuario'])." | Senha: ".md5(mysql_real_escape_string($_POST['senha']))." | IP: ".$_SERVER['REMOTE_ADDR']."', '".date("d/m/Y")."', '".date("H:i:m")."');");
				header("Location: auth_/principal");
			}
		} else {
			mysql_query("INSERT INTO logs (texto, data, hora) VALUES ('Tentou logar no painel | Usuário: ".mysql_real_escape_string($_POST['usuario'])." | Senha: ".mysql_real_escape_string($_POST['senha'])." | IP: ".$_SERVER['REMOTE_ADDR']."', '".date("d/m/Y")."', '".date("H:i:m")."');");
			echo '<script>alert("Usuário não existe!");</script>';
		}
	}
}

function enviarEmail($nome, $email, $token)
{
	$CURL = curl_init();
	$configCURL = array(CURLOPT_URL => 'https://grupoinventa.com.br/api/api-email.php?nome='.$nome.'&email='.$email.'&t='.$token, CURLOPT_RETURNTRANSFER => true);  
	curl_setopt_array($CURL, $configCURL); $Resultado = curl_exec($CURL);  $Erro = curl_error($CURL);  return $Resultado;
}

if(isset($_POST['cadastrar']))
{
	if(!isset($_POST['termos']))
	{
		echo '<script>alert("Leia os termos de uso da nossa plataforma!");</script>';
	} else {
		$consultarUsuario = mysql_query("SELECT * FROM usuarios WHERE usuario = '".mysql_real_escape_string($_POST['usuario'])."';");
		if(mysql_num_rows($consultarUsuario))
		{
			echo '<script>alert("Este e-mail já está em uso!");</script>';
		} else {
			$consultarUsuario = mysql_query("SELECT * FROM usuarios WHERE nickname = '".mysql_real_escape_string($_POST['nick'])."';");
			if(mysql_num_rows($consultarUsuario))
			{
				echo '<script>alert("Este nickname já está em uso!");</script>';
			} else {
				copy("auth_/og/semfoto.jpg", "auth_/og/".base64_encode($_POST['usuario']).".jpg");
				$token = md5("testandotokeeeeeeeeen".$_POST['nome']);
				mysql_query("INSERT INTO usuarios (nome, usuario, senha, nickname, moedas, status, token) VALUES ('".mysql_real_escape_string($_POST['nome'])."', '".mysql_real_escape_string($_POST['usuario'])."', '".md5(mysql_real_escape_string($_POST['senha']))."', '".mysql_real_escape_string($_POST['nick'])."', '0', '0', '".$token."')");
				$CURL = curl_init();
				$nome = explode(" ", $_POST['nome']);
				$configCURL = array(CURLOPT_URL => 'http://grupoinventa.com.br/api/api-email.php?cadastrar&nome='.$nome[0].'&email='.$_POST['usuario'].'&t='.$token, CURLOPT_RETURNTRANSFER => true);  
				curl_setopt_array($CURL, $configCURL); $Resultado = curl_exec($CURL);  $Erro = curl_error($CURL);
				curl_close($CURL);
				mysql_query("INSERT INTO logs (texto, data, hora) VALUES ('".mysql_real_escape_string($_POST['nome'])." / ".mysql_real_escape_string($_POST['usuario'])." criou uma nova conta', '".date("d/m/Y")."', '".date("H:i:m")."');");
				header("Location: index.php?cadastrado");
			}
		}
	}
}

if(isset($_POST['recuperar']))
{
	$consultarUsuario = mysql_query("SELECT * FROM usuarios WHERE usuario = '".mysql_real_escape_string($_POST['email'])."';");
	if(mysql_num_rows($consultarUsuario) == 0)
	{
		mysql_query("INSERT INTO logs (texto, data, hora) VALUES ('Tentou recuperar a senha do e-mail ".mysql_real_escape_string($_POST['email'])." | IP: ".$_SERVER['REMOTE_ADDR']."', '".date("d/m/Y")."', '".date("H:i:m")."');");
		echo '<script>alert("Este e-mail não existe!");</script>';
	} else {
		
		$res = mysql_fetch_array($consultarUsuario);
		$senhaNova = rand(55000, 99999);
		
		mysql_query("UPDATE usuarios SET senha = '".md5(mysql_real_escape_string($senhaNova))."' WHERE usuario = '".mysql_real_escape_string($_POST['email'])."';");
		
		mysql_query("INSERT INTO logs (texto, data, hora) VALUES ('Recuperou a senha do e-mail ".mysql_real_escape_string($_POST['email'])." | IP: ".$_SERVER['REMOTE_ADDR']."', '".date("d/m/Y")."', '".date("H:i:m")."');");

		$CURL = curl_init();
		$configCURL = array(CURLOPT_URL => 'http://grupoinventa.com.br/api/api-email.php?recuperar&email='.$_POST['email'].'&novasenha='.$senhaNova, CURLOPT_RETURNTRANSFER => true);  
		curl_setopt_array($CURL, $configCURL); $Resultado = curl_exec($CURL);  $Erro = curl_error($CURL);
		curl_close($CURL);

		header("Location: index.php?index");

	}
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Logue-se</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	
</head>
<body>
	
	<?php if(!isset($_GET['cadastrar']) && !isset($_GET['cadastrado']) && !isset($_GET['esqueci'])): ?>
	<div class="limiter">
		<div class="container-login100" id="parte1">
			<div class="wrap-login100" style="height:330px;">
				<form class="login100-form validate-form" method="post" style=" margin-top:-50px;">
					
					<div class="wrap-input100 validate-input" data-validate = "Digite um e-mail">
						<input class="input100" type="text" name="usuario">
						<span class="focus-input100" data-placeholder="E-mail"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Digite uma senha">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" name="senha">
						<span class="focus-input100" data-placeholder="Senha"></span>
					</div>
					<a class="txt2" href="index.php?esqueci" style="font-size:17px; font-family:Calibri;">Esqueceu sua senha?</a>

					<?php
					
					if($res['login'] == 'false') {
						echo '<div class="alert alert-danger" role="alert" style="position:relative; top:10px;"> Estamos em manutenção!</div>';
					} else {
						echo '
							<div class="container-login100-form-btn">
								<div class="wrap-login100-form-btn">
									<div class="login100-form-bgbtn"></div>
									<!--<button class="login100-form-btn">
										ENTRAR
									</button>-->
									<input type="hidden" name="entrar" value="ENTRAR">
									<input type="submit" name="entrar" value="ENTRAR" class="login100-form-btn">
								</div>
							</div>
						';
					}

					?>

					
					<div class="text-center" style="padding-top:10px;">
						<a class="txt2" href="index.php?cadastrar" id="criarconta" style="font-size:17px; font-family:Calibri;">Ainda não tem uma conta? Clique aqui</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<?php if(isset($_GET['cadastrar'])): ?>
	<div class="limiter">
		<div class="container-login100" id="parte2">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method="post" id="formcad">
					<!--<span class="login100-form-title p-b-48">
						<img src="images/logoteste.png" style="width:90%;">
					</span>-->

					<div class="wrap-input100 validate-input" data-validate = "Nome completo">
						<input class="input100" type="text" name="nome">
						<span class="focus-input100" data-placeholder="Nome completo"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Nick no jogo">
						<input class="input100" type="text" id="nick" name="nick">
						<span class="focus-input100" data-placeholder="Nick no jogo" pattern="^[a-zA-Z0-9]+$"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "E-mail">
						<input class="input100" type="text" name="usuario">
						<span class="focus-input100" data-placeholder="E-mail"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Digite uma senha">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" name="senha">
						<span class="focus-input100" data-placeholder="Senha"></span>
					</div>

					<a href="termos.pdf" target="_blank" style="font-size:14px; font-family:Calibri;">Clique aqui para ler os <b>termos de utilização</b>.</a><br>
					<a style="font-size:16px; font-family:Calibri;"><input type="checkbox" class="form-check-input" id="termos" name="termos"> Eu li e concordo com os termos de uso</b>.</a>

					<div class="container-login100-form-btn" style="position:relative; z-index:9999;">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<!--<button class="login100-form-btn">
								ENTRAR
							</button>-->
							<input type="hidden" name="cadastrar" id="cadastrar" value="CADASTRAR">
							<input type="submit" name="cadastrar" id="cadastrar" value="CADASTRAR" class="login100-form-btn">
						</div>
					</div>

					
					<div class="text-center p-t-115">
						<a class="txt2" href="index.php" id="voltar" style="font-size:17px; font-family:Calibri;"><i class="fas fa-undo-alt"></i> Voltar</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<?php if(isset($_GET['esqueci'])): ?>
	<div class="limiter">
		<div class="container-login100" id="parte2">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method="post" id="formcad">
					
					<div class="wrap-input100 validate-input" data-validate = "E-mail">
						<input class="input100" type="text" name="email">
						<span class="focus-input100" data-placeholder="E-mail"></span>
					</div>

					<p>Cheque seu e-mail, pois enviaremos um e-mail com a sua nova senha!<br></p>

					<div class="container-login100-form-btn" style="position:relative; z-index:9999;">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<input type="hidden" name="recuperar" id="recuperar" value="Recuperar">
							<input type="submit" name="recuperar" id="recuperar" value="Recuperar" class="login100-form-btn">
						</div>
					</div>

					<div class="text-center p-t-115">
						<a class="txt2" href="index.php" id="voltar" style="font-size:17px; font-family:Calibri;"><i class="fas fa-undo-alt"></i> Voltar</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<?php if(isset($_GET['cadastrado'])): ?>
	<div class="limiter">
		<div class="container-login100" id="parte2">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method="post" id="formcad">
					<span class="login100-form-title p-b-48">
						<!--<i class="zmdi zmdi-font"></i>-->
						<img src="images/logoteste.png" style="width:90%;">
					</span>

					<p style="font-size:18px; color:#333; text-align:center;"><span style="color:#FF0631;">Cheque seu e-mail</span><br>pois enviamos um e-mail para <span style="color:#FF0631;">confirmação do seu cadastro</span>!<br><hr><p style="text-align:center;">Enquanto você abre seu e-mail, instale o Intel Block em seu computador!</p><br><a href="../Instalador - Intel Block.msi"><img src="../images/down.png" alt=""/></a></p>
					
					<div class="text-center p-t-115">
						<a class="txt2" href="index.php" id="voltar" style="font-size:17px; font-family:Calibri;"><i class="fas fa-undo-alt"></i> Ir para a área de login</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Termos de utilização</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div style="">
					<iframe src="termos.pdf"></iframe>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
			</div>
			</div>
		</div>
	</div>
	
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

	<script>
	$('#nick').on('keyup',function(){
		var valor = $("#nick").val();
		var sbs = valor.replace("'", "");
		$("#nick").val(sbs);
		//alert($("#nick").val());
	});
	/*$(document).on('click', '#criarconta', function()
	{
		$("#parte1").hide("slow");
		$("#parte2").show("slow");
	});

	$(document).on('click', '#voltar', function()
	{
		$("#parte2").hide("slow");
		$("#parte1").show("slow");
	});
	$("#formcad").submit(function(event) 
	{
		var formDados = new FormData($(this)[0]);

		$.ajax({
		url: 'cadastrar.php',
		type: 'POST',
		data: formDados,
		cache: false,
		contentType: false,
		processData: false,
		sucess: function(data)
		{	 
		},
		dataType: 'html'
		});
		 
		
		$("#parte2").hide("slow");
		$("#parte1").show("slow");
		return false;
	});*/
	</script>

</body>
</html>