<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="folha.css">
    <title>Login</title>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css'><script src='https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js'></script><link rel='stylesheet' href='https://cdn.jsdelivr.net/foundation/6.2.0/foundation.min.css'><link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
    <style class="cp-pen-styles">@import url(https://fonts.googleapis.com/css?family=Montserrat);
    @import url(https://fonts.googleapis.com/css?family=Open+Sans);
    input[type=password]::-ms-reveal, input[type=password]::-ms-clear {
    display: none;
    }

    /* Removing this since I'll add it myself for browser compatibility */
    input[type="email"], input[type="password"], input[type="submit"] {
    -webkit-border-top-left-radius: 360px;
    -moz-border-top-left-radius: 360px;
    -ms-border-top-left-radius: 360px;
    border-top-left-radius: 360px;
    -webkit-border-top-right-radius: 360px;
    -moz-border-top-right-radius: 360px;
    -ms-border-top-right-radius: 360px;
    border-top-right-radius: 360px;
    -webkit-border-bottom-left-radius: 360px;
    -moz-border-bottom-left-radius: 360px;
    -ms-border-bottom-left-radius: 360px;
    border-bottom-left-radius: 360px;
    -webkit-border-bottom-right-radius: 360px;
    -moz-border-bottom-right-radius: 360px;
    -ms-border-bottom-right-radius: 360px;
    border-bottom-right-radius: 360px;
    }

    input[type="email"], input[type="password"] {
    padding-left: 2.75em;
    }

    input[type="submit"] {
    width: 100%;
    background-color: #003b64;
    color: white;
    border: 0;
    padding: 8px;
    -webkit-transition: all 0.25s ease-in-out;
    -moz-transition: all 0.25s ease-in-out;
    -ms-transition: all 0.25s ease-in-out;
    -o-transition: all 0.25s ease-in-out;
    transition: all 0.25s ease-in-out;
    }

    input[type="submit"]:hover {
    background-color: #1677bb;
    }

    label {
    display: inline;
    position: absolute;
    top: 0.65em;
    left: 2.30em;
    color: #999999;
    }

    form {
    position: relative;
    top: 50%;
    -webkit-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    transform: translateY(-50%);
    }

    form .row .row {
    margin: 0 -0.9375rem;
    }

    body {
    color: #999999;
    background-image: url("http://www.migloo.be/labs/lab1/img/background.jpg");
    background-size: cover;
    }

    p.intro1, p.intro2 {
    text-align: center;
    color: white;
    font-size: 0.9em;
    margin-bottom: 40px;
    font-family: 'Open Sans';
    }

    p.intro2 {
    opacity: 0;
    }

    .wrapper, .morphHeader {
    font-family: "Montserrat", Helvetica, Arial, sans-serif;
    background-color: #f2f2f2;
    -webkit-border-top-left-radius: 6px;
    -moz-border-top-left-radius: 6px;
    -ms-border-top-left-radius: 6px;
    border-top-left-radius: 6px;
    -webkit-border-top-right-radius: 6px;
    -moz-border-top-right-radius: 6px;
    -ms-border-top-right-radius: 6px;
    border-top-right-radius: 6px;
    -webkit-border-bottom-left-radius: 6px;
    -moz-border-bottom-left-radius: 6px;
    -ms-border-bottom-left-radius: 6px;
    border-bottom-left-radius: 6px;
    -webkit-border-bottom-right-radius: 6px;
    -moz-border-bottom-right-radius: 6px;
    -ms-border-bottom-right-radius: 6px;
    border-bottom-right-radius: 6px;
    }

    .header, .footer, .morphHeader {
    text-align: center;
    text-transform: uppercase;
    }

    .header, .morphHeader {
    -webkit-border-top-left-radius: 6px;
    -moz-border-top-left-radius: 6px;
    -ms-border-top-left-radius: 6px;
    border-top-left-radius: 6px;
    -webkit-border-top-right-radius: 6px;
    -moz-border-top-right-radius: 6px;
    -ms-border-top-right-radius: 6px;
    border-top-right-radius: 6px;
    -webkit-border-bottom-left-radius: 0;
    -moz-border-bottom-left-radius: 0;
    -ms-border-bottom-left-radius: 0;
    border-bottom-left-radius: 0;
    -webkit-border-bottom-right-radius: 0;
    -moz-border-bottom-right-radius: 0;
    -ms-border-bottom-right-radius: 0;
    border-bottom-right-radius: 0;
    font-size: 1.7em;
    background-image: url("http://www.migloo.be/labs/lab1/img/backgroundBox.png");
    padding: 2em;
    color: white;
    }

    .username {
    padding-top: 1.25rem;
    }

    .submit, .footer {
    padding-bottom: 1.25rem;
    }

    .morphButton {
    z-index: 999;
    -webkit-border-top-left-radius: 360px;
    -moz-border-top-left-radius: 360px;
    -ms-border-top-left-radius: 360px;
    border-top-left-radius: 360px;
    -webkit-border-top-right-radius: 360px;
    -moz-border-top-right-radius: 360px;
    -ms-border-top-right-radius: 360px;
    border-top-right-radius: 360px;
    -webkit-border-bottom-left-radius: 360px;
    -moz-border-bottom-left-radius: 360px;
    -ms-border-bottom-left-radius: 360px;
    border-bottom-left-radius: 360px;
    -webkit-border-bottom-right-radius: 360px;
    -moz-border-bottom-right-radius: 360px;
    -ms-border-bottom-right-radius: 360px;
    border-bottom-right-radius: 360px;
    }

    .morphButton, .morphHeader {
    opacity: 0;
    background-color: #1677bb;
    text-align: center;
    }

    .loading, .success, .failure {
    color: white;
    line-height: 34px !important;
    text-align: center;
    }

    .tooltip p {
    font-family: 'Open Sans';
    font-size: 13px;
    line-height: 16px;
    }

    .columns {
    position: relative;
    }
    </style>
</head>
<body>
    <form method="post" autocomplete="off">
        <!--<p class="intro1">First try to log with anything to see a <strong style="color: #ff3322">failed</strong> login&hellip;</p>-->
        <p class="intro2">Now try to log with "john@doe.com" and "test" to see a <strong style="color: #a4c639">successful</strong> login!</p>

        <div class="row">
            <div class="wrapper large-4 columns large-centered small-6 small-centered">
                <div class="row header">
                    <div class="large-12 columns">LOGIN</div>
                </div>

                <div class="row username">
                    <div class="large-9 columns large-centered">
                        <label for="username"><i class="fa fa-user"></i></label>
                        <input id="username" type="email" name="username" placeholder="Seu e-mail" required />
                    </div>
                </div>

                <div class="row password">
                    <div class="large-9 columns large-centered">
                        <label for="password"><i class="fa fa-lock"></i></label>
                        <input id="password" type="password" name="pass" placeholder="Sua senha" required autocomplete="off" />
                    </div>
                </div>

                <div class="row submit">
                    <div class="large-9 columns large-centered">
                        <input type="submit" value="ENTRAR">
                    </div>
                </div>
            </div>
        </div>
    </form>

    <span class='morphButton'>
        <img class="avatar" src="http://www.migloo.be/labs/lab1/img/avatar.png" alt="John... Doe?">
        <i class="fa fa-refresh fa-spin loading"></i>
        <i class="fa fa-check success"></i>
        <i class="fa fa-times failure"></i>
    </span>

    <span class="morphHeader"><span>SIGN IN</span></span>

    <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js'></script>
    <script src="js.js"></script>
    
    
</body>
</html>