﻿<!DOCTYPE html>

<?php

include("painel/conn.php");

$pg = mysql_query("SELECT * FROM gerenciamento");
$res = mysql_fetch_array($pg);

if($res['status'] == 'false')
{
    header("Location: _404/");
}

?>

<html lang="en">
    
    <head>
        <meta charset=utf-8>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Intel Block</title>
        <!-- Load Roboto font -->
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    
        <!-- Load css styles -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css?<?php echo rand(0, 9999); ?>" />
        <link rel="stylesheet" type="text/css" href="css/pluton.css" />
        <!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="css/pluton-ie7.css" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="css/jquery.cslider.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
        <link rel="stylesheet" type="text/css" href="css/animate.css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
        
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57.png">
        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <!-- Add jQuery library -->
<script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>



    </head>
    
    <body>

        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                     
                        <img src="images/logo.png" width="200" alt="Logo" style="margin-top:7px;"/>
                    
                    <!-- Navigation button, visible on small resolution -->
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <i class="icon-menu"></i>
                    </button>
                    <!-- Main navigation -->
                    <div class="nav-collapse collapse pull-right">
                        <ul class="nav" id="top-navigation" style="z-index:9999;">
                            <li class="active"><a href="#home">Home</a></li>
                            <li><a href="#servico">Serviços</a></li>
                            <li><a href="#planos">Planos</a></li>
                            <li><a href="#download">Download</a></li>
                            <li><a href="painel/index.php?cadastrar">Cadastro</a></li>
                            <li><a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal">Banidos</a></li>
                            <li><a href="#contato">Contato</a></li>
                             <li><a href="painel/">Entrar</a></li>
                        </ul>
                    </div>
                    <!-- End main navigation -->
                </div>
            </div>
        </div>
        <!-- Start home section -->
        <div id="home">
            <!-- Start cSlider -->
            <div id="da-slider" class="da-slider">
               
                <!-- mask elemet use for masking background image -->
                <div class="mask" style="background-image: url(images/mask.jpg); background-size:cover; background-attachment:fixed;"></div>
                <!-- All slides centred in container element -->
                <div class="container">
                    <!-- Start first slide -->
                    <div class="da-slide">
                        <!--<h2 class="fittext2" id="titulo"><img src="images/titulo.png"></h2>-->
                        <div style="width:700px; height:auto; display:table; margin:0 auto; margin-top:110px;">
                        <img src="images/intel.png" alt="image01">
                        </div>
                        <!--<div class="da-img" style="width:500px;">
                            <img src="images/gif.gif" alt="image01">
                        </div>-->
                    </div>
                    <!-- End first slide -->
                    <!-- Start second slide -->
      
                    </div>
                    <!-- Start third slide -->
                    <!-- Start cSlide navigation arrows -->
                    <div class="da-arrows">
                        <span class="da-arrows-prev"></span>
                        <span class="da-arrows-next"></span>
                    </div>
                    <!-- End cSlide navigation arrows -->
                </div>
            </div>
        </div>
        <!-- End home section -->
        <!-- Service section start -->
        <div class="section primary-section" id="servico">
            <div class="container">
                <!-- Start title section -->
                <div class="title">
                    <h1>Quem somos</h1>
                    <!-- Section's title goes here -->
                    <p>
                    O <span style="color:white;">Intel Block</span> é um anti-cheater desenvolvido com o objetivo de oferecer <span style="color:white;">segurança aos eventos de e-Sports</span>, monitorando, transmitindo informações e bloqueando programas ilegais.<br>
                    Quando o software detecta uma trapaça na máquina de um jogador, o acesso do mesmo é suspenso temporáriamente, para que a equipe de análise possa verificar todas as informações coletadas, ao mesmo tempo, emite um comunicado imediato ao organizador do evento, informando que o jogador foi suspenso para análise. 
                    <br><br>
                    <span style="color:white;">#goIB</span> #goIntelBlock <span style="color:white;">#eSports</span> #gamers<br>
                    <span style="color:white;">Qualidade</span> e <span style="color:white;">Segurança</span>, este é nosso compromisso!
                    </p>
                    <!--Simple description for section goes here. -->
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                                <img class="img-circle" src="images/Service1.png" alt="service 1">
                            </div>
                            <h3>Sinta-se em casa</h3>
                            <p>Com o nosso software, voce pode ficar tranquilo!</p>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                                <img class="img-circle" src="images/Service2.png" alt="service 2" />
                            </div>
                            <h3>Preços</h3>
                            <p>Nossos preços são acessíveis a todos.</p>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="centered service">
                            <div class="circle-border zoom-in">
                                <img class="img-circle" src="images/Service3.png" alt="service 3">
                            </div>
                            <h3>Segurança</h3>
                            <p>Além do monitoramento, contamos com um módulo para detecção de programas ilegais.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Service section end -->
      
        
    </div>
        <!-- Price section start -->
        <div id="planos" class="section secondary-section" style="background-color:white;">
            <div class="container">
                <div class="title">
                    <h1>Planos</h1>
                    <p>Conheça nossos planos!</p>
                </div>
                <div class="price-table row-fluid">
                    <div class="span3 price-column">
                        <h3>Basic</h3>
                        <ul class="list">
                            <li class="price">R$ 15,99</li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>20 Partidas</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Monitoramento de processos</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Monitoramento por screenshot</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Bloqueio de programas ilegais</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Capt. histórico de navegação</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Capt. histórico de downloads</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Painel de controle</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Suporte técnico</strong></li>
                            <form action="https://pagseguro.uol.com.br/checkout/v2/payment.html" method="post" onsubmit="PagSeguroLightbox(this); return false;">
                                <input type="hidden" name="code" value="7FC90594EBEBBED444EC3FA925899E23" />
                                <input type="hidden" name="iot" value="button" />
                                <input type="submit" name="submit" class="btn btn-lg btn-block btn-danger" value="Comprar agora" style="margin-top:20px; padding:10px; margin-bottom:-30px;">
                            </form>
                        </ul>
                        
                    </div>
                    <div class="span3 price-column">
                        <h3>PLUS</h3>
                        <ul class="list">
                            <li class="price">R$ 25,99</li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>30 Partidas</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Monitoramento de processos</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Monitoramento por screenshot</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Bloqueio de programas ilegais</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Capt. histórico de navegação</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Capt. histórico de downloads</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Painel de controle</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Suporte técnico</strong></li>
                            <form action="https://pagseguro.uol.com.br/checkout/v2/payment.html" method="post" onsubmit="PagSeguroLightbox(this); return false;">
                                <input type="hidden" name="code" value="FC8EFAF2A7A75ADDD4615FBDDD72576E" />
                                <input type="hidden" name="iot" value="button" />
                                <input type="submit" name="submit" class="btn btn-lg btn-block btn-danger" value="Comprar agora" style="margin-top:20px; padding:10px; margin-bottom:-30px;">
                            </form>
                        </ul>
                    </div>
                    <div class="span3 price-column">
                        <h3>max</h3>
                        <ul class="list">
                            <li class="price">R$ 45,99</li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>100 Partidas</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Monitoramento de processos</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Monitoramento por screenshot</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Bloqueio de programas ilegais</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Capt. histórico de navegação</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Capt. histórico de downloads</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Painel de controle</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Suporte técnico</strong></li>
                            <form action="https://pagseguro.uol.com.br/checkout/v2/payment.html" method="post" onsubmit="PagSeguroLightbox(this); return false;">
                                <input type="hidden" name="code" value="B203266DF9F9333EE43D7F812BF4098D" />
                                <input type="hidden" name="iot" value="button" />
                                <input type="submit" name="submit" class="btn btn-lg btn-block btn-danger" value="Comprar agora" style="margin-top:20px; padding:10px; margin-bottom:-30px;">
                            </form>
                        </ul>
                    </div>
                    <div class="span3 price-column">
                        <h3>TIER</h3>
                        <ul class="list">
                            <li class="price">R$ 95,99</li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>30 dias</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Monitoramento de processos</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Monitoramento por screenshot</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Bloqueio de programas ilegais</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Capt. histórico de navegação</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Capt. histórico de downloads</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Painel de controle</strong></li>
                            <li><i class="fas fa-check" style="color:green; padding:5px;"></i><strong>Suporte técnico</strong></li>
                            <form action="https://pagseguro.uol.com.br/checkout/v2/payment.html" method="post" onsubmit="PagSeguroLightbox(this); return false;">
                                <input type="hidden" name="code" value="0F6CBC578E8EBE8444A2AF890AEFB09D" />
                                <input type="hidden" name="iot" value="button" />
                                <input type="submit" name="submit" class="btn btn-lg btn-block btn-danger" value="Comprar agora" style="margin-top:20px; padding:10px; margin-bottom:-30px;">
                            </form>
                        </ul>
                    </div>
                </div>
                <!--<div class="centered">
                    <p class="price-text">Está na duvida em qual plano escolher? Entre em contato conosco!</p>
                    <a href="#contact" class="button">Contate nós</a>
                </div>-->
            </div>
        </div>
        <!-- Price section end -->
         <div class="section primary-section" id="download">
            <div class="container">
                <!-- Start title section -->
                <div class="title">
                    <h1>Faça já o download do nosso anti-cheater</h1>
                    <!-- Section's title goes here -->
                    <p>Requisitos mínimos:</p>
                    <h5 style="color:white;">Sistema Operacional: <strong>Windows 7, 8 e 10</strong></h5>
                    <h5 style="color:white;">NET Framework: <strong>Versão 4.5.2 ou superior</strong></h5>
                    <!--Simple description for section goes here. -->
                </div>
                <center>
                  <a href="Instalador - Intel Block.msi"><img src="images/down.png" alt=""/></a>
                  </center>
                
           </div>
        </div>
        
         
        <!-- Newsletter section start -->
        <div id="contato">
        <div class="section third-section">
            <div class="container newsletter">
                <div class="sub-section">
                    <div class="title clearfix">
                        <div class="pull-left">
                            <h3>Contato</h3>
                        </div>
                    </div>
                </div>
                <div id="success-subscribe" class="alert alert-success invisible">
                    <strong>Well done!</strong>You successfully subscribet to our newsletter.</div>
                <div class="row-fluid">
                    <div class="span5">
                        <p>Para entrar em contato entre em nosso servidor Teamspeak!</p>
                        <h2><a style="color:white;" href="ts3server://suporte.intelblock.com.br?port=&nickname=Intel Block - Usuário"><i class="fab fa-teamspeak"></i> suporte.intelblock.com.br</a></h2>
                        <p>Ou então nos acompanhe em nossas redes sociais!</p>
                        <ul class="social">
                            <li>
                                <a href="https://www.facebook.com/IntelBlock/" target="_blank" title="Facebook">
                                    <span class="fab fa-facebook-square" style="color:white;"></span>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/channel/UCZxDgtXK0SGFCK1cq_Q4iHw" target="_blank" title="Youtube">
                                    <span class="fab fa-youtube" style="color:white;"></span>
                                </a>
                            </li>
                            <li>
                                <a href="https://discord.gg/eW5P5hZ" target="_blank" title="Discord">
                                    <span class="fab fa-discord" style="color:white;"></span>
                                </a>
                            </li>
                        </ul>

                        
                    </div>
                    <div class="span7">
                      <div id="err-subscribe" class="error centered">Please provide valid email address.</div>
                  </div>
                </div>
            </div>
            </div>
        </div>
        <!-- Newsletter section end -->
    <!-- Contact section start -->
    
        <!-- Contact section edn -->
        <!-- Footer section start -->
        <div class="footer">
            <p>Apoio<br><img src="images/ts3host.png" style="width:14%;"><br>&copy; 2018 Intel Block. Todos os direitos reservados.<br>CNPJ : 31.566.314/0001-80</p>
        </div>
        <!-- Footer section end -->
        <!-- ScrollUp button start -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="color:#333; font-size:17px;">Lista de usuários banidos</h5>
                </div>
                <div class="modal-body">
                <?php
                    $query = mysql_query("SELECT * FROM bans ORDER BY id DESC");
                    if(mysql_num_rows($query) == 0)
                    {
                        echo '<div class="alert alert-secondary" role="alert">Não há jogadores banidos!</div>';
                    } else {
                        echo '
                        <table class="table table-striped table-sm" style="color:#333;">
                            <thead>
                                <tr>
                                    <th scope="col" style="font-size: 14px;">Jogador(a)</th>
                                    <th scope="col" style="font-size: 14px;">Banimento</th>
                                    <th scope="col" style="font-size: 14px;">Desbanimento</th>
                                    <th scope="col" style="font-size: 14px;">Motivo</th>
                                    <th scope="col" style="font-size: 14px;">Jogo</th>
                                </tr>
                            </thead>
                            <tbody>';
                                while($res = mysql_fetch_array($query))
                                {
                                    echo '
                                    <tr>
                                        <td style="font-size: 14px;">'.$res['nickname'].'</td>
                                        <td style="font-size: 14px;">'.$res['banimento'].'</td>
                                        <td style="font-size: 14px;">'.$res['desbanimento'].'</td>
                                        <td style="font-size: 14px;">'.$res['motivo'].'</td>
                                        <td style="font-size: 14px;">'.$res['jogo'].'</td>
                                    </tr>';
                                }
                            echo '
                            </tbody>
                        </table>
                        ';
                    }
                ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
                </div>
            </div>
        </div>

        <div class="scrollup">
            <a href="#">
                <i class="icon-up-open"></i>
            </a>
        </div>

        <!--<div class="modal fade" id="aviso" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="color:#333; font-size:20px;">Aviso</h5>
                </div>
                <div class="modal-body" style="color:#333;">
                    Se você já possui o Intel Block e está enfrentando problemas, baixe o novo instalador <a href="Instalador - Intel Block.msi" style="color:#FF0631;">clicando aqui</a> ou na área de downloads.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
                </div>
            </div>
        </div>-->

        <!-- ScrollUp button end -->
        <!-- Include javascript -->
         
        <script src="js/jquery.js"></script>
        
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <script type="text/javascript" src="js/app.js"></script>
        <script>
			$('#aviso').modal('show');
		</script>
    </body>
</html>