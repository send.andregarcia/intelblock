<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv = "Compatível com X-UA" content = "IE = EmulateIE8" />
    <script src="jquery-3.3.1.min.js"></script>
    <script src="jquery.cycle2.min.js"></script>

    <style> 
        * { margin:0 auto; padding:0; }
        body { background-color: #202020; }
        ul {
            list-style:none;
        }
        li { 
            color:white;
            font-family:Calibri;
            font-size:16px;
        }
        .img { 
            width:100%;
            height:314px;
        }
    </style>
</head>
<body>
    <!-- slideshow with one image -->
    <div class="cycle-slideshow" 
            data-cycle-fx="scrollHorz" 
            data-cycle-timeout="2000"
        >
        <div class="cycle-caption"></div>
        <img src="1.jpg" class="img">
        <img src="2.jpg" class="img">
    </div>

    <script>
    var images = [
        '<img src="http://malsup.github.io/images/p2.jpg">',
        '<img src="http://malsup.github.io/images/p3.jpg">',
        '<img src="http://malsup.github.io/images/p4.jpg">'
    ];

    $('button').one('click', function() {
        for (var i=0; i < images.length; i++) {
            $('.cycle-slideshow').cycle('add', images[i]);
        }
        $(this).prop('disabled', true)
    })
    </script>
</body>
</html>